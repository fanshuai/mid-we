var util = require('util');
var express = require('express');
var request = require('superagent');
var oauth_sign = require('../library/oauth-sign');
var oauth_util = require('../library/oauth-util');

var router = express.Router();

router.param(function(name, fn){
    if (fn instanceof RegExp) {
        return function(req, res, next, val){
            if (fn.test(String(val))) {
                req.params[name] = val;
                next();
            } else {
                next('route');
            }
        }
    }
});

router.param('type', /^(weibo|douban|google|github|facebook)$/);

router.get('/type/:type', function(req, res) {
    if (req.session.user) {
        res.redirect('/');
    } else {
        req.session.redirect_oauth_type = req.params.type;
        res.redirect(oauth_util.oauth_sgin_url[req.params.type]);
    }
});

router.get('/weibo', function(req, res) {
    if (req.query.error || !req.query.code) {
        req.session.oauth_error = util.format(
            'weibo oauth error: %s', req.query.error
        );
        res.redirect('/');
    } else {
        req.session.user = {
            oauth_type: 'weibo', oauth_code: req.query.code
        };
        oauth_sign.sign_weibo_profile(req);
        res.redirect('/');
    }
});

router.get('/douban', function(req, res) {
    if (req.query.error || !req.query.code) {
        req.session.oauth_error = util.format(
            'douban oauth error: %s', req.query.error
        );
        res.redirect('/');
    } else {
        req.session.user = {
            oauth_type: 'douban', oauth_code: req.query.code
        };
        oauth_sign.sign_douban_profile(req);
        res.redirect('/');
    }
});

router.get('/google', function(req, res) {
    if (req.query.error || !req.query.code) {
        req.session.oauth_error = util.format(
            'google oauth error: %s', req.query.error
        );
        res.redirect('/');
    } else {
        req.session.user = {
            oauth_type: 'google', oauth_code: req.query.code
        };
        oauth_sign.sign_google_profile(req);
        res.redirect('/');
    }
});

router.get('/github', function(req, res) {
    if (req.query.error || !req.query.code) {
        req.session.oauth_error = util.format(
            'github oauth error: %s', req.query.error
        );
        res.redirect('/');
    } else {
        req.session.user = {
            oauth_type: 'github', oauth_code: req.query.code
        };
        oauth_sign.sign_github_profile(req);
        res.redirect('/');
    }
});

router.get('/facebook', function(req, res) {
    if (req.query.error || !req.query.code) {
        req.session.oauth_error = util.format(
            'facebook oauth error: %s', req.query.error
        );
        res.redirect('/');
    } else {
        req.session.user = {
            oauth_type: 'facebook', oauth_code: req.query.code
        };
        oauth_sign.sign_facebook_profile(req);
        res.redirect('/');
    }
});

module.exports = router;