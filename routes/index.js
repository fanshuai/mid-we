var redis = require('redis');
var express = require('express');
var router = express.Router();

var config = require('../config');
var debug = require('debug')('midwe');

var redis_sign_out_pub = redis.createClient(
    config.redis.port, config.redis.host, config.redis.options
);

/* GET home page. */
router.get('/', function(req, res) {
    req.session['user-agent'] = req.headers['user-agent'];
    if (req.session.user) {
        res.render('index', {});
    } else {
        res.render('sign-in', {oauth_error: req.session.oauth_error});
        delete req.session.oauth_error;
    }
    req.session.save();
});

router.get('/sign-out', function(req, res) {
    redis_sign_out_pub.publish('user.sign-out', JSON.stringify(
        {pid: process.pid, sid: req.session.id}
    ));
    req.session.destroy();
    res.redirect('/');
});


module.exports = router;
