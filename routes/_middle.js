var url = require('url');
var debug = require('debug')('midwe');
module.exports = function (app) {

    app.use('/', function (req, res, next) {
        if (req.path == '/auth-ing') {
            res.redirect('/');
        } else if (req.path.indexOf('/oauth/') == 0) {
            next();
        } else if (req.path.indexOf('/static/') == 0) {
            next();
        } else if (req.path.indexOf('/gen204') == 0) {
            next();  // for Google Translate script
        } else if (req.path.indexOf('/apple-touch-icon.png') == 0) {
            res.redirect('/static/images/apple-touch-icon.png');  // for Safari browser
        } else {
            req.session.next_url = req.url;
            res.redirect('/');
        }
    });

    /// catch 404 and forwarding to error handler
    app.use('/', function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handlers
    // development error will print stacktrace
    // production error no stacktraces leaked to user
    // noinspection JSUnusedLocalSymbols
    app.use('/', function (err, req, res, next) {
        err.status = err.status || 500;
        res.status(err.status);
        debug(req.connection.remoteAddress, err.status);
        res.render('error', {
            url: req.url,
            status: err.status,
            message: err.message,
            stack: app.get('env') === 'development' ? err.stack : ''
        });
    });

};