var geoip = require('geoip-lite');
var cookieParser = require('cookie-parser');
var redisAdapter = require('socket.io-redis');

var config = require('../config');
var debug = require('debug')('midwe:socket');
var parseCookie = cookieParser(config.session.secret);


var redis_session_sub = require('redis').createClient(
    config.redis.port, config.redis.host, config.redis.options
);

redis_session_sub.on('error', function (err) {
    console.log('Redis error', err);
});

redis_session_sub.on('ready', function () {
    redis_session_sub.subscribe('oauth.success');
    redis_session_sub.subscribe('user.sign-out');
});

module.exports = function (io) {

    io.set('transports', [
        'websocket'
        , 'flashsocket'
        , 'htmlfile'
        , 'xhr-polling'
        , 'jsonp-polling'
        , 'polling'
    ]);

    io.adapter(redisAdapter(config.redis));

    io.use(function(socket, next) {
        socket.user_ip = socket.request.headers['x-forwarded-for'];
        if (!socket.user_ip) socket.user_ip = socket.request.connection.remoteAddress;
        if (socket.request.xdomain || !socket.request.headers.cookie) {
            // xdomain && cookie TODO
            next(new Error('Authorization Error'));
        } else {
            parseCookie(socket.request, null, function(err) {
                if (err) {
                    next(new Error('Cookie parse error'));
                } else {
                    socket.sid = socket.request.signedCookies[config.session.key];
                    var geo_info = geoip.lookup(socket.user_ip);
                    if (!geo_info && (process.env.NODE_ENV === 'production')) {
                        geo_info = {ll: [37.774929, -122.419416], city: socket.user_ip, country: 'UN'};
                    } else if (!geo_info){
                        geo_info = geoip.lookup('218.241.203.198');
                    }
                    socket.geo_info = {
                        ip: socket.user_ip,
                        country: geo_info.country,
                        region: geo_info.region || geo_info.country,
                        city: geo_info.city || socket.user_ip,
                        lat: geo_info.ll[0],
                        lng: geo_info.ll[1]
                    };
                    debug('WS connect from:', socket.geo_info.ip, 'with pid:', process.pid);
                    next();
                }
            });
        }
    });

    io.of('/stream').use(function(socket, next) {
        if (socket.request.xdomain || !socket.request.headers.cookie) {
            // xdomain && cookie TODO
            next(new Error('Authorization Error'));
        } else {
            parseCookie(socket.request, null, function(err) {
                if (err) {
                    next(new Error('Cookie parse error'));
                } else {
                    socket.sid = socket.request.signedCookies[config.session.key];
                    next();
                }
            });
        }
    });

    redis_session_sub.on("message", function (channel, message) {
        var data = JSON.parse(message);
        if (data.sid && (process.pid == data.pid)) {
            if (channel == 'oauth.success') {
                io.to('sess:' + data.sid).emit('oauth.success', '');
            } else if (channel == 'user.sign-out') {
                io.to('sess:' + data.sid).emit('user.sign-out', '');
            }
        }
    });

};