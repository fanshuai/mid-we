var path = require('path');
var util = require('util');
var request = require('request');
var mongoose = require('mongoose');
var ss = require('socket.io-stream');
var Grid = require('gridfs-stream');
var config = require('../config');

var gm = require('gm').subClass({ imageMagick: true });

var FsFiles = require('../model').FsFiles;
var conn = mongoose.createConnection(
    util.format('mongodb://%s/%s', config.mongodb.host, config.mongodb.database)
);
//conn.once('open', function () {
//    var gfs = Grid(conn.db, mongoose.mongo);
//});

// make sure the db instance is open before passing into `Grid`
var gfs = Grid(conn.db, mongoose.mongo);

module.exports = function (io) {

    io.of('/stream').on('connection', function (socket) {

        ss(socket).on('uploadImage', function (stream, data, fn) {
            // stream.pipe(fs.createWriteStream(filename));
            var metadata = {type: data.type, sid: socket.sid};
            var filename = path.basename(data.name);
            var extname = path.extname(data.name) == '.gif' ? 'gif' : 'jpg';
            gm(stream).size({bufferStream: true}, function (err, size) {
                if (!err) {
                    metadata.width = size.width;
                    metadata.height = size.height;
                }
            }).stream(extname, function (err, stdout, stderr) {
                if (err) {
                    return fn && fn({ error: String(err)});
                } else if (stderr) {
                    // console.log(stderr);
                }
                var writestream = gfs.createWriteStream({
                    filename: '.' + extname,
                    metadata: metadata,
                    aliases: filename
                });
                stdout.pipe(writestream);
                writestream.on('error', function (err) {
                    console.log('gfs write error:', uri, err);
                });
                writestream.on('close', function (file) {
                    fn && fn({_id: file._id});
                });
            });
        });

        ss(socket).on('image', function (stream, data) {
            var readstream = gfs.createReadStream({_id: data._id});
            readstream.on('error', function (err) {
                console.log('image error:', data, err);
            });
            readstream.pipe(stream);
        });

        socket.on('remove', function (fid, fn) {
            FsFiles.findById(fid, function (err, doc) {
                if (!err && doc && doc.metadata && doc.metadata.sid==socket.sid) {
                    gfs.remove({_id: fid}, function (err) {
                        if (err) return fn && fn({error: err});
                        fn && fn({_id: fid});
                    });
                }
            });
        });

    });

};



//socket.on('readOne', function (fid, fn) {
//    var base_string = '';
//    var readstream = gfs.createReadStream({_id: fid});
//    readstream.on('error', function (err) {
//        console.log('gfs read error:', fid, err);
//    });
//    readstream.on('data', function(chunk) {
//        base_string += chunk.toString('base64');
//    });
//    readstream.on('end', function () {
//        fn(base_string);
//    });
//});


//ss(socket).on('read', function (stream, uri) {
//    var readstream = fs.createReadStream(
//        path.join(config.static_dir, uri.replace('/static', ''))
//    );
//    readstream.on('error', function (err) {
//        console.log('read error:', uri, err);
//    });
//    readstream.pipe(stream);
//});