var fs = require('fs');
var url = require('url');
var path = require('path');
var async = require('async');
var lodash = require('lodash');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var model = require('../model');
var config = require('../config');
var debug = require('debug')('midwe:socket');
var api_util = require('../library/api-util');
var figure_util = require('../library/figure-util');

var User = model.User;
var Topic = model.Topic;
var Comment = model.Comment;
var Message = model.Message;
var OAuthProfile = model.OAuthProfile;
var FsFiles = model.FsFiles;
var store = new RedisStore(config.redis);
var page_size = config.page_size || 10;

var model_server = require('../model/model-server');


module.exports = function (server) {
    var io = require('socket.io')(server);

    require('./_middle')(io);

    require('./ws-stream')(io);

    io.on('connection', function (socket) {
        var sid = socket.sid;
        socket.join('sess:' + sid);

        socket.on('console.out', function(msg, fn) {
            fs.readFile(path.join(config.static_dir, 'console.out'), 'utf8', function (err, data){
                if (!err) fn && fn(data);
            });
        });

        socket.on('console', function(msg, fn) {
            if (!msg) {
                async.parallel([
                    function (callback) {
                        User.count({status: 'ok', is_online: true}, callback);
                    },
                    function (callback) {
                        User.count({status: 'ok'}, callback);
                    }
                ], function (err, results) {
                    if (err) {
                        fn && fn(JSON.stringify({pid: process.pid, geo_info: socket.geo_info}));
                    } else {
                        fn && fn(JSON.stringify({pid: process.pid, geo_info: socket.geo_info, on_line: results}));
                    }
                });
            } else {
                socket.broadcast.emit('console', socket.geo_info.city + '[' + socket.user_ip + ']: ' + msg);
                fn && fn(socket.geo_info.city + '[' + socket.user_ip + '][me]: ' + msg);
            }
        });

        socket.on('user.sign-in', function(msg, fn) {
            store.get(sid, function (err, session) {
                if (!err && session && session.user && session.user.id) {
                    socket.uid = session.user.id;
                    User.findById(socket.uid)
                        .select('-updated_at -social_network')
                        .lean().exec(function(err, user){
                            if (err || !user) return fn && fn({error: err});
                            socket.join('user:' + socket.uid);
                            socket.join('city:' + socket.geo_info.city);
                            user.geo_info = socket.geo_info;
                            fn && fn({
                                user: user,
                                page_size: page_size,
                                next_url: session.next_url
                            });
                            delete session.next_url;
                            session.geo_info = socket.geo_info;
                            store.set(sid, session, function (err) {
                                model_server.save_sign_in_log(sid, session);
                                if (err) console.warn('user.sign-in session save error', sid, err);
                            });
                            Message.count({to_user: socket.uid, status: 'ok', read_time: null}, function (err, count) {
                                if (!err)  io.to('user:' + socket.uid).emit('message.unread', count);
                            });
                        }
                    );
                } else if (!err && session && session.user && session.user.oauth_type) {
                    fn && fn({oauth_type: session.user.oauth_type});
                } else {
                    fn && fn({pid: process.pid});
                }
            });
        });

        socket.on('i-am-here', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.accuracy) return fn && fn({error: 'data params error'});
            User.findById(socket.uid)
                .select('username nickname avatar')
                .lean().exec(function(err, user){
                    if (err || !user) return fn && fn({error: err});
                    fn && fn(data);
                    delete data.accuracy;
                    if (lodash.isNumber(data.lat) && lodash.isNumber(data.lng)) {
                        data.lat = Number(data.lat.toFixed(5));
                        data.lng = Number(data.lng.toFixed(5));
                    }
                    io.to('city:' + socket.geo_info.city).emit('i-am-here', {
                        user: user, loc_info: data, city: socket.geo_info.city,
                        created_at: new Date(data.timestamp)
                    });
                }
            );
        });

        socket.on('topic.add', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.title || !data.content) return fn && fn({error: 'data params error'});
            data.title = data.title.replace(/ +(?= )/g,'');
            data.content = data.content.replace(/(<([^>]+)>)/ig, '');
            if (!data.title || !data.content) return fn && fn({error: 'data params error'});
            new Topic({
                user: socket.uid,
                title: data.title,
                content: data.content,
                images: data.images,
                loc_info: data.loc_info,
                geo_info: socket.geo_info
            }).save(
                function (err, topic) {
                    if (err) return fn({error: err});
                    Topic.findById(topic._id).select(
                        'user title created_at images geo_info loc_info.lat loc_info.lng comments likes'
                    ).populate(
                        'user', 'nickname avatar'
                    ).lean().exec(
                        function (err, doc) {
                            if (err) return fn && fn({error: err});
                            fn && fn(doc); io.to('city:' + socket.geo_info.city).emit('topic.stream', doc);
                            if (lodash.isArray(data.images)) for (var i=0; i<data.images.length; i++) {
                                FsFiles.findById(data.images[i], function(err, file) {
                                    if (!err && file) {
                                        file.filename = 'topic_' + topic._id + file.filename;
                                        file.metadata.uid = socket.uid;
                                        file.markModified('metadata');
                                        file.save();
                                    }
                                });
                            }
                        }
                    );
                }
            );
        });

        socket.on('topic.get', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data._id) return fn && fn({error: 'data params error'});
            Topic.findOne({_id: data._id, status: 'ok'}).select(
                'user title content created_at images geo_info loc_info.lat loc_info.lng comments views'
            ).populate('user', 'nickname avatar').lean().exec(
                function(err, doc) {
                    if (err) return fn && fn({error: err});
                    if (doc && !(doc.user._id == socket.uid)) {
                        Topic.findByIdAndUpdate(doc._id, { $inc: { views: 1} }, function (err, doc) {
                            io.to('topic:' + doc._id).emit('topic.views', doc.views);
                        });
                    }
                    fn && fn(doc);
                }
            );
        });

        socket.on('topic.likes', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data._id) return fn && fn({error: 'data params error'});
            Topic.findOne({_id: data._id, status: 'ok'}, function(err, topic) {
                if (err) return fn && fn({error: err});
                if (!topic) return fn && fn({up: 0, down: 0, type: ''});
                if (data.type=='up') {
                    if (topic.likes.up.indexOf(socket.uid) < 0) {
                        topic.likes.up.push(socket.uid);
                    } else {
                        topic.likes.up.splice(topic.likes.up.indexOf(socket.uid), 1);
                    }
                    if (topic.likes.down.indexOf(socket.uid) > -1) {
                        topic.likes.down.splice(topic.likes.down.indexOf(socket.uid), 1);
                    }
                } else if (data.type=='down') {
                    if (topic.likes.down.indexOf(socket.uid) < 0) {
                        topic.likes.down.push(socket.uid);
                    } else {
                        topic.likes.down.splice(topic.likes.down.indexOf(socket.uid), 1);
                    }
                    if (topic.likes.up.indexOf(socket.uid) > -1) {
                        topic.likes.up.splice(topic.likes.up.indexOf(socket.uid), 1);
                    }
                }

                var like_data = {up: topic.likes.up.length, down: topic.likes.down.length, type: ''};
                if (topic.likes.up.indexOf(socket.uid) > -1) {
                    like_data.type = 'up';
                } else if (topic.likes.down.indexOf(socket.uid) > -1) {
                    like_data.type = 'down';
                }
                fn && fn(like_data);

                if (['up', 'down'].indexOf(data.type) > -1) {
                    io.to('topic:' + topic._id).emit('topic.likes.change', '');
                    if (figure_util.is_topic_hate(like_data)) {
                        topic.status = 'hate';
                    }
                    topic.markModified('likes.up');
                    topic.markModified('likes.down');
                    topic.save();
                }
            });
        });

        socket.on('topic.comments', function(data, fn) {
            if (!data || !data._id) return fn && fn({error: 'data params error'});
            Topic.findOne({_id: data._id, status: 'ok'}).select('comments').lean().exec(
                function(err, doc) {
                    if (!err && doc) fn && fn(doc.comments);
                }
            );
        });

        socket.on('topic.list', function(data, fn) {
            if (!data) return fn && fn({error: 'data params error'});
            var topic_query = Topic.find(
                {'geo_info.city': socket.geo_info.city, status: 'ok'}
            );
            if (data.search) {
                topic_query = topic_query.or([
                    {title: new RegExp(data.search, 'i')},
                    {content: new RegExp(data.search, 'i')}
                ]);
            }
            if (data.last_id && lodash.isNumber(data.last_id)) {
                topic_query = topic_query.find({_id: {$lt: data.last_id}});
            }
            topic_query.select(
                'user title created_at images geo_info loc_info.lat loc_info.lng comments views'
            ).populate(
                'user', 'nickname avatar'
            ).sort('-_id').limit(page_size).lean().exec(
                function(err, docs) {
                    if (err) return fn && fn({error: err});
                    fn && fn(docs);
                }
            );
        });

        socket.on('topic.bound', function(data, fn) {
            if (!data || !data._southWest || !data._northEast) return fn && fn({error: 'data params error'});
            Topic.find({ status: 'ok',
                'loc_info.lat': { $gt: data._southWest.lat, $lt: data._northEast.lat },
                'loc_info.lng': { $gt: data._southWest.lng, $lt: data._northEast.lng }
            }).select(
                'title loc_info.lat loc_info.lng geo_info.city'
            ).sort('-_id').lean().exec(
                function(err, docs) {
                    if (err) return fn && fn({error: err});
                    fn && fn(docs);
                }
            );
        });

        socket.on('topic.buzz', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.topic || !data.content) return fn && fn({error: 'data params error'});
            data.content = data.content.replace(/(<([^>]+)>)/ig, '');
            if (!data.content) return fn && fn({error: 'data params error'});
            User.findById(socket.uid)
                .select('username nickname avatar')
                .lean().exec(function(err, user){
                    if (err || !user) return fn && fn({error: err});
                    fn && fn(data);
                    data.user = user;
                    data.created_at = new Date();
                    data.city = socket.geo_info.city;
                    io.to('topic:' + data.topic).emit('topic.buzz', data);
                }
            );
        });

        socket.on('comment.add', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.content) return fn && fn({error: 'data params error'});
            data.content = data.content.replace(/(<([^>]+)>)/ig, '');
            if (!data.content) return fn && fn({error: 'content is empty'});
            Topic.findOne({_id: data.topic, status: 'ok'}).select('comments').lean().exec(
                function(err, doc) {
                    if (err || !doc) return fn && fn({error: err});
                    if (doc.comments > 999) return fn && fn({error: 'limit number'});

                    new Comment({
                        user: socket.uid,
                        topic: data.topic,
                        content: data.content,
                        loc_info: data.loc_info,
                        geo_info: socket.geo_info
                    }).save(
                        function (err, comment) {
                            if (err) return fn && fn({error: err});
                            Comment.findById(comment._id).select(
                                'user topic content created_at geo_info loc_info.lat loc_info.lng'
                            ).populate(
                                'user', 'nickname avatar'
                            ).lean().exec(
                                function (err, doc) {
                                    if (err) return fn && fn({error: err});
                                    fn && fn(doc); io.to('topic:' + comment.topic).emit('topic.comment', doc);
                                }
                            );
                            Comment.count({topic: data.topic, status: 'ok'}, function (err, count) {
                                if (err) return;
                                io.to('topic:' + comment.topic).emit('topic.comments', count);
                                Topic.findByIdAndUpdate(comment.topic, {comments: count}).exec();
                            });
                        }
                    );

                }
            );
        });

        socket.on('comment.likes', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data._id) return fn && fn({error: 'data params error'});
            Comment.findOne({_id: data._id, status: 'ok'}, function(err, comment) {
                if (err) return fn && fn({error: err});
                if (!comment) return fn && fn({up: 0, down: 0, type: ''});
                if (data.type=='up') {
                    if (comment.likes.up.indexOf(socket.uid) < 0) {
                        comment.likes.up.push(socket.uid);
                    } else {
                        comment.likes.up.splice(comment.likes.up.indexOf(socket.uid), 1);
                    }
                    if (comment.likes.down.indexOf(socket.uid) > -1) {
                        comment.likes.down.splice(comment.likes.down.indexOf(socket.uid), 1);
                    }
                } else if (data.type=='down') {
                    if (comment.likes.down.indexOf(socket.uid) < 0) {
                        comment.likes.down.push(socket.uid);
                    } else {
                        comment.likes.down.splice(comment.likes.down.indexOf(socket.uid), 1);
                    }
                    if (comment.likes.up.indexOf(socket.uid) > -1) {
                        comment.likes.up.splice(comment.likes.up.indexOf(socket.uid), 1);
                    }
                }

                var like_data = {up: comment.likes.up.length, down: comment.likes.down.length, type: ''};
                if (comment.likes.up.indexOf(socket.uid) > -1) {
                    like_data.type = 'up';
                } else if (comment.likes.down.indexOf(socket.uid) > -1) {
                    like_data.type = 'down';
                }
                fn && fn(like_data);

                if (['up', 'down'].indexOf(data.type) > -1) {
                    if (figure_util.is_comment_hate(like_data)) {
                        io.to('topic:' + comment.topic).emit('comment.hate', {_id: comment._id});
                        comment.status = 'hate';
                    }
                    comment.markModified('likes.up');
                    comment.markModified('likes.down');
                    comment.save(function () {
                        Comment.count({topic: comment.topic, status: 'ok'}, function (err, count) {
                            if (!err && lodash.isNumber(count)) {
                                io.to('topic:' + comment.topic).emit('topic.comments', count);
                                Topic.findByIdAndUpdate(comment.topic, {comments: count}).exec();
                            }
                        });
                    });
                }
            });
        });

        socket.on('comment.list', function(data, fn) {
            if (!data || !data.topic) return fn && fn({error: 'data params error'});
            Comment.find(
                {'topic': data.topic, status: 'ok'}
            ).select(
                'user content created_at geo_info loc_info.lat loc_info.lng'
            ).populate(
                'user', 'nickname avatar'
            ).sort('_id').lean().exec(
                function(err, docs) {
                    if (err) return fn && fn({error: err});
                    socket.join('topic:' + data.topic);
                    io.to('topic:' + data.topic).emit('topic.user.change', '');
                    fn && fn(docs);
                }
            );
        });

        socket.on('topic.user', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.topic) return fn && fn({error: 'data params error'});
            var user_ids = [];
            var topic_room = 'topic:' + data.topic;
            var io_adapter = io.nsps[socket.nsp.name].adapter;
            var sid_dict = io_adapter.rooms[topic_room] || {};

            for (var sid in sid_dict) {
                if (sid_dict.hasOwnProperty(sid)) {
                    var conn_uid = (io_adapter.nsp.connected[sid] || {}).uid;
                    if (conn_uid && !(conn_uid == socket.uid) && user_ids.indexOf(conn_uid) < 0) {
                        user_ids.push(conn_uid);
                    }
                }
            }

            User.find(
                {'_id': {$in: user_ids}, status: 'ok'}
            ).select(
                'username nickname sign_oauth_type avatar'
            ).lean().exec(
                function(err, docs) {
                    if (err) return fn && fn({error: err});
                    fn && fn(docs);
                }
            );
        });

        socket.on('comment.leave', function(data, fn) {
            if (!data || !data.topic) return fn && fn({error: 'data params error'});
            socket.leave('topic:' + data.topic);
            io.to('topic:' + data.topic).emit('topic.online.change', '');
            fn && fn(data);
        });

        socket.on('me.topic.list', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data) return fn && fn({error: 'data params error'});
            async.parallel([
                function (callback) {
                    Topic.count({user: socket.uid, status: 'ok'}, callback);
                },
                function (callback) {
                    Topic.find({user: socket.uid, status: 'ok'})
                        .select('title created_at geo_info comments views')
                        .sort('-_id').skip(page_size * ((data.page - 1) || 0)).limit(page_size)
                        .lean().exec(callback);
                }
            ], function (err, results) {
                if (err || !results) {
                    fn && fn({error: err});
                } else if (!lodash.isArray(results) || !(results.length == 2)) {
                    fn && fn({error: 'results error'});
                } else if (!lodash.isNumber(results[0]) || !lodash.isArray(results[1])) {
                    fn && fn({error: 'results error'});
                } else {
                    fn && fn({count: results[0], list: results[1]});
                }
            });
        });

        socket.on('me.comment.list', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data) return fn && fn({error: 'data params error'});
            async.parallel([
                function (callback) {
                    Comment.count({user: socket.uid, status: 'ok'}, callback)
                },
                function (callback) {
                    Comment.find({user: socket.uid, status: 'ok'})
                        .select('topic content created_at geo_info')
                        .populate('topic', 'user title comments geo_info.city')
                        .sort('-_id').skip(page_size * ((data.page - 1) || 0)).limit(page_size)
                        .lean().exec(callback);
                }
            ], function (err, results) {
                if (err || !results) {
                    fn && fn({error: err});
                } else if (!lodash.isArray(results) || !(results.length == 2)) {
                    fn && fn({error: 'results error'});
                } else if (!lodash.isNumber(results[0]) || !lodash.isArray(results[1])) {
                    fn && fn({error: 'results error'});
                } else {
                    fn && fn({count: results[0], list: results[1]});
                }
            });
        });

        socket.on('message.send', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data) return fn && fn({error: 'data params error'});
            data.content = (data.content  || '').replace(/(<([^>]+)>)/ig, '');
            if (!data.content && !data.image) return fn && fn({error: 'data params error'});
            User.findOne({_id: data.to_user, status: 'ok'})
                .select('username nickname avatar')
                .lean().exec(function(err, to_user){
                    if (err || !to_user) return fn && fn({error: err});
                    new Message({
                        user: socket.uid,
                        to_user: to_user._id,
                        content: data.content,
                        image: data.image || null,
                        city: socket.geo_info.city
                    }).save(
                        function (err, message) {
                            if (err) return fn && fn({error: err});
                            Message.findById(message._id).select(
                                'user to_user content created_at status image city read_time'
                            ).populate(
                                [{path:'user', select:'nickname avatar'}, {path:'to_user', select:'nickname avatar'}]
                            ).lean().exec(
                                function (err, doc) {
                                    if (err) return fn && fn({error: err});
                                    fn && fn(doc);
                                    io.to('user:' + socket.uid).emit('message.send', doc);
                                    if (socket.uid != to_user._id) io.to('user:' + to_user._id).emit('message.send', doc);
                                }
                            );
                            Message.count({to_user: to_user._id, status: 'ok', read_time: null}, function (err, count) {
                                if (!err) io.to('user:' + to_user._id).emit('message.unread', count);
                            });
                            Message.update({user: data.to_user, to_user: socket.uid, read_time: null}, {read_time: new Date()}, {multi: true}, function (err, numberAffected, raw) {
                                if (!err && numberAffected > 0) {
                                    if (!raw) console.log('The raw response from Mongo was ', raw);
                                    Message.count({to_user: socket.uid, status: 'ok', read_time: null}, function (err, count) {
                                        if (!err)  io.to('user:' + socket.uid).emit('message.unread', count);
                                    });
                                }
                            });
                            if (lodash.isString(data.image)) {
                                FsFiles.findById(data.image, function(err, file) {
                                    if (!err && file) {
                                        file.filename = 'message_' + message._id + file.filename;
                                        file.metadata.uid = socket.uid;
                                        file.markModified('metadata');
                                        file.save();
                                    }
                                });
                            }
                        }
                    );
                }
            );
        });

        socket.on('message.list', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data || !data.to_user) return fn && fn({error: 'data params error'});
            var message_query = Message.find({status: 'ok'}).or([
                {user: socket.uid, to_user: data.to_user},
                {user: data.to_user, to_user: socket.uid}
            ]);
            if (data.last_id && lodash.isNumber(data.last_id)) {
                message_query = message_query.find({_id: {$lt: data.last_id}});
            }
            message_query.select(
                'user to_user content created_at status image city read_time'
            ).populate(
                [{path:'user', select:'nickname avatar'}, {path:'to_user', select:'nickname avatar'}]
            ).sort('-_id').limit(page_size).lean().exec(
                function(err, docs) {
                    if (err) return fn && fn({error: err});
                    Message.update({user: data.to_user, to_user: socket.uid, read_time: null}, {read_time: new Date()}, {multi: true}, function (err, numberAffected, raw) {
                        if (!err && numberAffected > 0) {
                            if (!raw) console.log('The raw response from Mongo was ', raw);
                            Message.count({to_user: socket.uid, status: 'ok', read_time: null}, function (err, count) {
                                if (!err)  io.to('user:' + socket.uid).emit('message.unread', count);
                            });
                        }
                    });
                    fn && fn(docs);
                }
            );
        });

        socket.on('message.to_user', function(data, fn) {
            if (!data || !data.to_user) return fn && fn({error: 'data params error'});
            User.findOne({_id: data.to_user, status: 'ok'})
                .select('nickname avatar sign_oauth_type sign_oauth_home')
                .lean().exec(function(err, user){
                    if (err || !user) return fn && fn({error: err});
                    fn && fn(user);
                }
            );
        });

        socket.on('message.unread', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            var unread_query = {to_user: socket.uid, status: 'ok', read_time: null};
            if (data && data.to_user && lodash.isNumber(data.to_user)) { unread_query.user = data.to_user; }
            Message.count(unread_query, function (err, count) {
                fn && (err ? fn({error: err}) : fn(count));
            });
        });

        socket.on('message.contact', function(data, fn) {
            if (!socket.uid) return fn && fn({error: 'user session error'});
            if (!data) return fn && fn({error: 'data params error'});
            async.parallel([
                function (callback) {
                    Message.find({status: 'ok'}).or([
                        {user: socket.uid}, {to_user: socket.uid}
                    ]).distinct('contact_key').exec(callback);
                },
                function (callback) {
                    Message.aggregate([
                        { $match: { $or: [{ user: socket.uid }, { to_user: socket.uid }]} },
                        { $group: {
                            _id: '$contact_key',
                            user: { $last: '$user' },
                            to_user: { $last: '$to_user' },
                            content: { $last: '$content' },
                            created_at: { $last: '$created_at' },
                            image: { $last: '$image' },
                            city: { $last: '$city' },
                            read_time: { $last: '$read_time' },
                            total: { $sum: 1 }
                        }},
                        { $project : {
                            _id: '$contact_key',
                            user: '$user',
                            to_user: '$to_user',
                            content: '$content',
                            created_at: '$created_at',
                            image: '$image',
                            city: '$city',
                            read_time: '$read_time',
                            total: '$total'
                        }},
                        { $sort: { created_at: -1 } },
                        { $skip: page_size * ((data.page - 1) || 0) },
                        { $limit: page_size }
                    ], callback);
                }
            ], function (err, results) {
                if (err || !results) {
                    fn && fn({error: err});
                } else if (!lodash.isArray(results) || !(results.length == 2)) {
                    fn && fn({error: 'results error'});
                } else if (!lodash.isArray(results[0]) || !lodash.isArray(results[1])) {
                    fn && fn({error: 'results error'});
                } else {
                    fn && fn({count: results[0].length, list: results[1]});
                }
            });
        });

        socket.on('osm.reverse', function(data, fn) {
            api_util.code_lat_lng(data).success(function (code_info) {
                fn && fn(code_info);
            }).error(function (err) {
                debug('code_lat_lng err: ', err);
                fn && fn({ error: err });
            });
        });

        socket.on('disconnect', function () {
            model_server.offline_sign_in_log(sid, socket.user_ip);
        });

    });
};