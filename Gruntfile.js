module.exports = function(grunt){

    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: [
            'build/fonts',
            'build/images',
            'build/requirejs',
            'build/templates',
            'build/stylesheets'
        ],

        copy: {
            main: {
                files: [
                    {expand: true, cwd: 'public/', src: 'robots.txt', dest: 'build/'},
                    {expand: true, cwd: 'public/', src: 'favicon.ico', dest: 'build/'},
                    {expand: true, cwd: 'public/', src: 'console.out', dest: 'build/'},
                    {expand: true, cwd: 'public/images/', src: '**', dest: 'build/images/'},
                    {expand: true, cwd: 'public/templates/', src: '**', dest: 'build/templates/'},
                    {expand: true, cwd: 'public/stylesheets/', src: 'error.css', dest: 'build/stylesheets/'},
                    {expand: true, cwd: 'public/stylesheets/', src: 'midwe.*.css', dest: 'build/stylesheets/'},
                    {expand: true, cwd: 'public/bower/requirejs/', src: 'require.min.js', dest: 'build/', filter: 'isFile'},
                    {expand: true, cwd: 'public/bower/requirejs/', src: 'require.min.js', dest: 'public/', filter: 'isFile'},
                    {expand: true, cwd: 'public/bower/bootstrap/dist/fonts/', src: '*', dest: 'build/fonts/', filter: 'isFile'},
                    {expand: true, cwd: 'public/bower/bootstrap/dist/fonts/', src: '*', dest: 'public/fonts/', filter: 'isFile'}
                ]
            }
        },

        cssmin: {
            minify: {
                options: {
                    banner: '/* <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> minified */'
                },
                expand: true,
                cwd: 'build/stylesheets/',
                src: ['*.css', '!*.min.css'],
                dest: 'build/stylesheets/',
                ext: '.min.css'
            },
            combine: {
                options: {
                    keepSpecialComments: 0,
                    banner: '/* <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> minified */'
                },
                files: {
                    'public/stylesheets/midwe.main.css': [
                        'public/bower/bootstrap/dist/css/bootstrap.min.css',
                        'public/bower/angular-motion/dist/angular-motion.min.css',
                        'public/bower/leaflet-dist/leaflet.css',
                        'public/bower/Leaflet.label/dist/leaflet.label.css',
                        'public/bower/leaflet.markercluster/dist/MarkerCluster.css',
                        'public/bower/leaflet.markercluster/dist/MarkerCluster.Default.css',
                        'public/stylesheets/loading.css',
                        'public/stylesheets/main.css'
                    ],
                    'public/stylesheets/midwe.sign.css': [
                        'public/bower/bootstrap/dist/css/bootstrap.min.css',
                        'public/stylesheets/sign.css'
                    ]
                }
            }
        },

        requirejs: {
            compile: {
                options: {
                    uglify: {
                        no_mangle: true
                    },
                    baseUrl: 'public',
                    mainConfigFile: 'public/midwe.main.js',
                    optimize: 'uglify',
                    name: 'midwe.main',
                    out: 'build/midwe.main.js',
                    preserveLicenseComments: false
                    // dir: 'build', modules: [{ name: 'main' }]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    grunt.registerTask('style', ['clean', 'cssmin:combine', 'copy']);
    grunt.registerTask('default', ['clean', 'cssmin:combine', 'copy', 'requirejs']);

};