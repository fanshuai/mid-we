var config = require('./config');

var path = require('path');
var swig = require('swig');
var logger = require('morgan');
var express = require('express');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var index = require('./routes/index');
var oauth = require('./routes/oauth');
var debug = require('debug')('midwe:app');

var app = express();

// view engine setup
swig.setDefaults({
    cache: false, //  'memory',
    locals: {
        env: app.get('env'),
        common: config.common,
        now: function () { return new Date(); }
    }
});
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');

app.use('/', logger('dev'));
app.use('/', require('serve-favicon')(config.static_dir + '/favicon.ico'));
app.use('/', require('./library/robots')(config.static_dir + '/robots.txt'));
app.use('/static/', express.static(config.static_dir));

app.use('/', bodyParser.json());
app.use('/', bodyParser.urlencoded({
    extended: true
}));
app.use('/', cookieParser(config.session.secret));

app.use('/', session({
    key: config.session.key,
    secret: config.session.secret,
    store: new RedisStore(config.redis),
    cookie: { maxAge: config.session.age_hour * 3600000 },
    proxy: true,
    resave: true,
    saveUninitialized: true
}));


app.use('/', index);
app.use('/oauth', oauth);

require('./routes/_middle')(app);


var server = require('http').createServer(app);

require('./socket')(server);

var port = Number(process.env.PORT || config.port) + 10 * Number(process.env.P_NUM || 0);


if (!module.parent) {
    server.listen(port);
    debug(':) server started with port', port, 'pid', process.pid);
}

exports.port = port;
exports.server = server;