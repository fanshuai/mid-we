var Q = require('q');
var util = require('util');
var redis = require('redis');
var lodash = require('lodash');
var request = require('superagent');
var config = require('../config');

var redis_client = redis.createClient(
    config.redis.port, config.redis.host, config.redis.options
);

redis_client.on('error', function (err) {
    console.log('Redis error', err);
});

var deferredExtend = function (deferred) {
    deferred.promise.success = function (fn) {
        deferred.promise.then(function (value) {
            fn(value);
        });
        return deferred.promise;
    };
    deferred.promise.error = function (fn) {
        deferred.promise.then(null, function (value) {
            fn(value);
        });
        return deferred.promise;
    };
    return deferred.promise;
};

exports.code_lat_lng = function (coords) {
    var deferred = Q.defer();
    if (!(coords && lodash.isNumber(coords.lat) && lodash.isNumber(coords.lng))) {
        deferred.reject(new Error('code_lat_lng params error'));
    } else {
        var lat = coords.lat.toFixed(5), lng = coords.lng.toFixed(5);
        var key = util.format('latlng:%s,%s', lat, lng);
        redis_client.hgetall(key, function (err, code_info) {
            if (!err && code_info) {
                redis_client.expire(key, config.openstreetmap.latlng_expire);
                return deferred.resolve(code_info);
            }
            request.get(config.openstreetmap.reverse_api)
                .query({format: 'json', zoom: 16, lat: lat, lon: lng, email: config.openstreetmap.email})
                .timeout(config.openstreetmap.api_get_timeout)
                .on('error', function (err) { deferred.reject(err); })
                .end(function (res) {
                    if (res.ok) {
                        var code_info = {
                            address: String(res.body.display_name).substring(0, String(res.body.display_name).lastIndexOf(',')),
                            city: (res.body.address.city || res.body.address.state)
                        };
                        deferred.resolve(code_info);
                        redis_client.hmset(key,
                            'address', code_info.address, 'city', code_info.city
                        );
                        redis_client.expire(key, config.openstreetmap.latlng_expire);
                    } else {
                        deferred.reject(new Error('code_lat_lng error'));
                    }
                }
            );
        });
    }

    return deferredExtend(deferred);
};