var Q = require('q');
var url = require('url');
var util = require('util');
var request = require('superagent');
var querystring = require('querystring');
var debug = require('debug')('midwe:oauth-util');
var oauth_cfg = require('../config').oauth;

exports.oauth_sgin_url = {
    weibo: oauth_cfg.weibo.auth_url + url.format({
        query: {
            client_id: oauth_cfg.weibo.appkey,
            redirect_uri: oauth_cfg.weibo.redirect_uri
        }
    }),
    douban: oauth_cfg.douban.auth_url + url.format({
        query: {
            client_id: oauth_cfg.douban.appkey,
            redirect_uri: oauth_cfg.douban.redirect_uri,
            response_type: 'code'
        }
    }),
    google: oauth_cfg.google.auth_url + url.format({
        query: {
            client_id: oauth_cfg.google.appkey,
            redirect_uri: oauth_cfg.google.redirect_uri,
            response_type: 'code',
            access_type: 'offline',
            scope: 'email profile',
            login_hint: 'email'
        }
    }),
    github: oauth_cfg.github.auth_url + url.format({
        query: {
            client_id: oauth_cfg.github.appkey,
            redirect_uri: oauth_cfg.github.redirect_uri,
            response_type: 'code',
            scope: 'user:email'
        }
    }),
    facebook: oauth_cfg.facebook.auth_url + url.format({
        query: {
            client_id: oauth_cfg.facebook.appkey,
            redirect_uri: oauth_cfg.facebook.redirect_uri,
            response_type: 'code',
            scope: 'email,public_profile'
        }
    })
};


exports.get_weibo_token = function (code) {
    var deferred = Q.defer();
    request.post(oauth_cfg.weibo.token_url).type('form')
        .send({
            client_id: oauth_cfg.weibo.appkey,
            client_secret: oauth_cfg.weibo.secret,
            redirect_uri: oauth_cfg.weibo.redirect_uri,
            grant_type: 'authorization_code',
            code: code
        }).end(function (res) {
            // debug('get_weibo_token: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(JSON.parse(res.text));
            } else {
                deferred.reject(new Error('get_weibo_token error'));
            }
        });
    return deferred.promise;
};

exports.get_weibo_profile = function (token_info) {
    var deferred = Q.defer();
    request.get(oauth_cfg.weibo.user_profile)
        .query({uid: token_info.uid, access_token: token_info.access_token})
        .end(function (res) {
            // debug('get_weibo_profile: ', res.status, res.body);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_weibo_profile error'));
            }
        });
    return deferred.promise;
};

exports.get_douban_token = function (code) {
    var deferred = Q.defer();
    request.post(oauth_cfg.douban.token_url).type('form')
        .set('Authorization', 'Bearer access_token')
        .send({
            client_id: oauth_cfg.douban.appkey,
            client_secret: oauth_cfg.douban.secret,
            redirect_uri: oauth_cfg.douban.redirect_uri,
            grant_type: 'authorization_code',
            code: code
        }).end(function (res) {
            // debug('get_douban_token: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_douban_token error'));
            }
        });
    return deferred.promise;
};

exports.get_douban_profile = function (token_info) {
    var deferred = Q.defer();
    request.get(oauth_cfg.douban.user_profile)
        .set('Authorization', 'Bearer ' + token_info.access_token)
        .end(function (res) {
            // debug('get_douban_profile: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_douban_profile error'));
            }
        });
    return deferred.promise;
};

exports.get_google_token = function (code) {
    var deferred = Q.defer();
    request.post(oauth_cfg.google.token_url).type('form')
        .send({
            client_id: oauth_cfg.google.appkey,
            client_secret: oauth_cfg.google.secret,
            redirect_uri: oauth_cfg.google.redirect_uri,
            grant_type: 'authorization_code',
            code: code
        }).end(function (res) {
            // debug('get_google_token: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_google_token error'));
            }
        });
    return deferred.promise;
};

exports.get_google_profile = function (token_info) {
    var deferred = Q.defer();
    request.get(oauth_cfg.google.user_profile)
        .set('Authorization', 'Bearer ' + token_info.access_token)
        .end(function (res) {
            // debug('get_google_profile: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_google_profile error'));
            }
        });
    return deferred.promise;
};

exports.get_github_token = function (code) {
    var deferred = Q.defer();
    request.post(oauth_cfg.github.token_url).type('form')
        .send({
            client_id: oauth_cfg.github.appkey,
            client_secret: oauth_cfg.github.secret,
            redirect_uri: oauth_cfg.github.redirect_uri,
            code: code
        }).end(function (res) {
            // debug('get_github_token: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_github_token error'));
            }
        });
    return deferred.promise;
};

exports.get_github_profile = function (token_info) {
    var deferred = Q.defer();
    request.get(oauth_cfg.github.user_profile)
        .set('Authorization', 'token ' + token_info.access_token)
        .end(function (res) {
            // debug('get_github_profile: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(res.body);
            } else {
                deferred.reject(new Error('get_github_profile error'));
            }
        });
    return deferred.promise;
};

exports.get_facebook_token = function (code) {
    var deferred = Q.defer();
    request.get(oauth_cfg.facebook.token_url)
        .query({
            client_id: oauth_cfg.facebook.appkey,
            client_secret: oauth_cfg.facebook.secret,
            redirect_uri: oauth_cfg.facebook.redirect_uri,
            code: code
        }).end(function (res) {
            // debug('get_facebook_token: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(querystring.parse(res.text));
            } else {
                deferred.reject(new Error('get_facebook_token error'));
            }
        });
    return deferred.promise;
};

exports.get_facebook_profile = function (token_info) {
    var deferred = Q.defer();
    request.get(oauth_cfg.facebook.user_profile)
        .set('Authorization', 'Bearer ' + token_info.access_token)
        .end(function (res) {
            // debug('get_facebook_profile: ', res.status, res.body, res.text);
            if (res.ok) {
                deferred.resolve(JSON.parse(res.text));
            } else {
                deferred.reject(new Error('get_facebook_profile error'));
            }
        });
    return deferred.promise;
};