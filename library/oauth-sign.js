var Q = require('q');
var redis = require('redis');
var oauth_util = require('../library/oauth-util');
var model_server = require('../model/model-server');

var model = require('../model');
var config = require('../config');
var debug = require('debug')('midwe:oath');

var User = model.User;
var OAuthProfile = model.OAuthProfile;

var redis_oauth_pub = redis.createClient(
    config.redis.port, config.redis.host, config.redis.options
);


exports.sign_weibo_profile = function (req) {
    var token_info;
    oauth_util.get_weibo_token(req.query.code)
        .then(function (data) {
            token_info = data;
            var uid = token_info.uid;
            return model_server.get_user_by_oauth_uid('weibo', uid, token_info);
        }).then(function (data) {
            if (data) {
                req.session.user.id = data._id;
                redis_oauth_pub.publish('oauth.success', JSON.stringify(
                    {pid: process.pid, sid: req.session.id}
                ));
                req.session.save();
                throw new Error('user_has_sign_up');
            } else {
                return oauth_util.get_weibo_profile(token_info);
            }
        }).then(function (data) {
            return model_server.save_weibo_oauth_profile(token_info, data);
        }).then(function (data) {
            if (data.user) {
                return model_server.get_user_by_id(data.user);
            } else {
                return model_server.save_user(data);
            }
        }).then(function (data) {
            req.session.user.id = data._id;
            redis_oauth_pub.publish('oauth.success', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            req.session.save();
        }).catch(function (err) {
            if (!(err.message == 'user_has_sign_up')) {
                delete req.session.user;
                req.session.oauth_error = 'OAuth with weibo failed';
                req.session.save();
                redis_oauth_pub.publish('user.sign-out', JSON.stringify(
                    { pid: process.pid, sid: req.session.id }
                ));
                console.log('***** oauth_weibo_profile error *****');
                console.log(err);
            }
        }).done();
};

exports.sign_douban_profile = function (req) {
    var token_info;
    oauth_util.get_douban_token(req.query.code)
        .then(function (data) {
            token_info = data;
            var uid = token_info.douban_user_id;
            return model_server.get_user_by_oauth_uid('douban', uid, token_info);
        }).then(function (data) {
            if (data) {
                req.session.user.id = data._id;
                redis_oauth_pub.publish('oauth.success', JSON.stringify(
                    {pid: process.pid, sid: req.session.id}
                ));
                req.session.save();
                throw new Error('user_has_sign_up');
            } else {
                return oauth_util.get_douban_profile(token_info);
            }
        }).then(function (data) {
            return model_server.save_douban_oauth_profile(token_info, data);
        }).then(function (data) {
            if (data.user) {
                return model_server.get_user_by_id(data.user);
            } else {
                return model_server.save_user(data);
            }
        }).then(function (data) {
            req.session.user.id = data._id;
            redis_oauth_pub.publish('oauth.success', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            req.session.save();
        }).catch(function (err) {
            if (!(err.message == 'user_has_sign_up')) {
                delete req.session.user;
                req.session.oauth_error = 'OAuth with douban failed';
                req.session.save();
                redis_oauth_pub.publish('user.sign-out', JSON.stringify(
                    { pid: process.pid, sid: req.session.id }
                ));
                console.log('***** oauth_douban_profile error *****');
                console.log(err);
            }
        }).done();
};

exports.sign_google_profile = function (req) {
    var token_info;
    oauth_util.get_google_token(req.query.code)
        .then(function (data) {
            token_info = data;
            return oauth_util.get_google_profile(token_info);
        }).then(function (data) {
            return model_server.save_google_oauth_profile(token_info, data);
        }).then(function (data) {
            if (data.user) {
                return model_server.get_user_by_id(data.user);
            } else {
                return model_server.save_user(data);
            }
        }).then(function (data) {
            req.session.user.id = data._id;
            redis_oauth_pub.publish('oauth.success', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            req.session.save();
        }).catch(function (err) {
            delete req.session.user;
            req.session.oauth_error = 'OAuth with google failed';
            req.session.save();
            redis_oauth_pub.publish('user.sign-out', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            console.log('***** oauth_google_profile error *****');
            console.log(err);
        }).done();
};

exports.sign_github_profile = function (req) {
    var token_info;
    oauth_util.get_github_token(req.query.code)
        .then(function (data) {
            token_info = data;
            return oauth_util.get_github_profile(token_info);
        }).then(function (data) {
            return model_server.save_github_oauth_profile(token_info, data);
        }).then(function (data) {
            if (data.user) {
                return model_server.get_user_by_id(data.user);
            } else {
                return model_server.save_user(data);
            }
        }).then(function (data) {
            req.session.user.id = data._id;
            redis_oauth_pub.publish('oauth.success', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            req.session.save();
        }).catch(function (err) {
            delete req.session.user;
            req.session.oauth_error = 'OAuth with github failed';
            req.session.save();
            redis_oauth_pub.publish('user.sign-out', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            console.log('***** oauth_github_profile error *****');
            console.log(err);
        }).done();
};

exports.sign_facebook_profile = function (req) {
    var token_info;
    oauth_util.get_facebook_token(req.query.code)
        .then(function (data) {
            token_info = data;
            return oauth_util.get_facebook_profile(token_info);
        }).then(function (data) {
            return model_server.save_facebook_oauth_profile(token_info, data);
        }).then(function (data) {
            if (data.user) {
                return model_server.get_user_by_id(data.user);
            } else {
                return model_server.save_user(data);
            }
        }).then(function (data) {
            req.session.user.id = data._id;
            redis_oauth_pub.publish('oauth.success', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            req.session.save();
        }).catch(function (err) {
            delete req.session.user;
            req.session.oauth_error = 'OAuth with facebook failed';
            req.session.save();
            redis_oauth_pub.publish('user.sign-out', JSON.stringify(
                { pid: process.pid, sid: req.session.id }
            ));
            console.log('***** oauth_facebook_profile error *****');
            console.log(err);
        }).done();
};