var topic_base_num = 32;
var comment_base_num = 16;

exports.is_topic_hate = function (like_data) {
    var count = like_data.up + like_data.down;
    if (count < topic_base_num) {
        return like_data.down > Math.round(topic_base_num / 2);
    } else {
        return like_data.down > like_data.up;
    }
};

exports.is_comment_hate = function (like_data) {
    var count = like_data.up + like_data.down;
    if (count < comment_base_num) {
        return like_data.down > Math.round(comment_base_num / 2);
    } else {
        return like_data.down > like_data.up;
    }
};