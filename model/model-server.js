var Q = require('q');
var path = require('path');
var util = require('util');
var request = require('request');
var mongoose = require('mongoose');
var Grid = require('gridfs-stream');

var model = require('../model');
var config = require('../config');
var debug = require('debug')('midwe');

var gm = require('gm').subClass({ imageMagick: true });

var conn = mongoose.createConnection(
    util.format('mongodb://%s/%s', config.mongodb.host, config.mongodb.database)
);

var gfs = Grid(conn.db, mongoose.mongo);

var User = model.User;
var OAuthProfile = model.OAuthProfile;

var SignInLog = model.SignInLog;

var save_user_avatar_image = function (user, avatar_url) {
    var deferred = Q.defer();
    var metadata = {width: 120, height: 120, user: user._id};
    var filename = 'user_' + user._id + '.jpg';
    gm(request(avatar_url), filename).resize('120', '120').stream('jpg', function (err, stdout, stderr) {
        if (err) {
            deferred.reject(err);
        } else if (stderr) {
            // console.log(stderr);
        }
        var writestream = gfs.createWriteStream({
            filename: filename,
            contentType: 'image/jpeg',
            metadata: metadata,
            aliases: avatar_url
        });
        stdout.pipe(writestream);
        writestream.on('close', function (file) {
            user.avatar = file._id;
            user.save();
            deferred.resolve(user);
        });
    });
    return deferred.promise;
};

exports.save_user = function (oauth_profile_info) {
    var deferred = Q.defer();
    var user_info = {
        nickname: oauth_profile_info.profile.nickname,
        email: oauth_profile_info.profile.email,
        gender: oauth_profile_info.profile.gender,
        sign_oauth_type: oauth_profile_info.type,
        sign_oauth_uid: oauth_profile_info.uid,
        sign_oauth_home: oauth_profile_info.profile.home_url,
        social_network: {}
    };
    new User(user_info).save(function (err, user) {
        if (err) {
            deferred.reject(err);
        } else {
            OAuthProfile.findByIdAndUpdate(oauth_profile_info._id, {user: user._id}, function () { });
            user.social_network[oauth_profile_info.type] = oauth_profile_info._id;
            save_user_avatar_image(user, oauth_profile_info.profile.avatar).then(function (user) {
                deferred.resolve(user);
            }).catch(function (err) {
                deferred.reject(err);
            }).done();
        }
    });
    return deferred.promise;
};

exports.get_user_by_id = function (_id) {
    var deferred = Q.defer();
    User.findById(_id, function (err, doc) {
        if (err) {
            deferred.reject(err);
        } else if (!doc) {
            deferred.reject(new Error(util.format('User %s not found', _id)));
        } else {
            deferred.resolve(doc);
        }
    });
    return deferred.promise;
};

exports.get_oauth_profile_by_id = function (_id) {
    var deferred = Q.defer();
    OAuthProfile.findById(_id, function (err, doc) {
        if (err) {
            deferred.reject(err);
        } else if (!doc) {
            deferred.reject(new Error(
                util.format('OAuthProfile %s not found', _id)
            ));
        } else {
            deferred.resolve(doc);
        }
    });
    return deferred.promise;
};

exports.get_user_by_oauth_uid = function (type, uid, token_info) {
    var deferred = Q.defer();
    OAuthProfile.findOne({type: type, uid: uid}, function (err, doc) {
        if (!err && doc && doc.user) {
            doc.token_info = {
                access_token: token_info.access_token,
                expires_in: token_info.expires_in
            };
            doc.token_updated = new Date();
            User.findById(doc.user, function (err, user) {
                if (!err && user) {
                    deferred.resolve(user);
                } else {
                    debug(util.format('User %s not found', _id));
                    deferred.resolve(false);
                }
            });
            doc.save();
        } else {
            debug(util.format('OAuth user %s %s not sign up', type, uid));
            deferred.resolve(false);
        }
    });
    return deferred.promise;
};

var save_oauth_profile = function (oauth_profile_obj) {
    var deferred = Q.defer();
    var oauth_profile_unique = { type: oauth_profile_obj.type, uid: oauth_profile_obj.uid };
    OAuthProfile.findOne(oauth_profile_unique, function (err, doc){
        if (err) {
            deferred.reject(err);
        } else if (doc) {
            deferred.resolve(doc);
        } else {
            new OAuthProfile(oauth_profile_obj).save(function (err, doc) {
                if (err) {
                    deferred.reject(err);
                } else {
                    deferred.resolve(doc);
                }
            });
        }
    });
    return deferred.promise;
};

exports.save_weibo_oauth_profile = function (token_info, oauth_profile) {
    var oauth_profile_obj = {
        type: 'weibo',
        uid: oauth_profile.idstr,
        token_info: {
            access_token: token_info.access_token,
            expires_in: token_info.expires_in
        },
        token_updated: Date.now(),
        profile: {
            nickname: oauth_profile.name,
            location: oauth_profile.location,
            desc: oauth_profile.description,
            avatar: oauth_profile.avatar_large,
            home_url: config.oauth.weibo.base_profile_url + oauth_profile.idstr,
            created_at: oauth_profile.created_at
        }
    };
    return save_oauth_profile(oauth_profile_obj);
};

exports.save_douban_oauth_profile = function (token_info, oauth_profile) {
    var oauth_profile_obj = {
        type: 'douban',
        uid: oauth_profile.id,
        token_info: {
            access_token: token_info.access_token,
            expires_in: token_info.expires_in
        },
        token_updated: Date.now(),
        profile: {
            nickname: oauth_profile.name,
            location: oauth_profile.loc_name,
            desc: oauth_profile.desc,
            avatar: oauth_profile.large_avatar,
            home_url: oauth_profile.alt,
            created_at: oauth_profile.created
        }
    };
    return save_oauth_profile(oauth_profile_obj);
};

exports.save_google_oauth_profile = function (token_info, oauth_profile) {
    var oauth_profile_obj = {
        type: 'google',
        uid: oauth_profile.id,
        token_info: {
            access_token: token_info.access_token,
            expires_in: token_info.expires_in
        },
        token_updated: Date.now(),
        profile: {
            nickname: oauth_profile.displayName,
            email: oauth_profile.emails[0].value,
            location: oauth_profile.language,
            desc: oauth_profile.tagline,
            avatar: (oauth_profile.image.url || '').replace('50', '120'),
            home_url: oauth_profile.url
        }
    };
    return save_oauth_profile(oauth_profile_obj);
};

exports.save_github_oauth_profile = function (token_info, oauth_profile) {
    var oauth_profile_obj = {
        type: 'github',
        uid: oauth_profile.id,
        token_info: {
            access_token: token_info.access_token
        },
        token_updated: Date.now(),
        profile: {
            nickname: oauth_profile.name,
            username: oauth_profile.login,
            email: oauth_profile.email,
            location: oauth_profile.location,
            avatar: oauth_profile.avatar_url,
            home_url: oauth_profile.html_url,
            created_at: oauth_profile.created_at
        }
    };
    return save_oauth_profile(oauth_profile_obj);
};

exports.save_facebook_oauth_profile = function (token_info, oauth_profile) {
    var oauth_profile_obj = {
        type: 'facebook',
        uid: oauth_profile.id,
        token_info: {
            access_token: token_info.access_token,
            expires_in: token_info.expires
        },
        token_updated: Date.now(),
        profile: {
            nickname: oauth_profile.name,
            email: oauth_profile.email,
            gender: oauth_profile.gender,
            location: oauth_profile.locale,
            home_url: oauth_profile.link,
            avatar: 'http://graph.facebook.com/' + oauth_profile.id + '/picture?height=120&width=120'
        }
    };
    return save_oauth_profile(oauth_profile_obj);
};

var get_sign_in_log = function (sid, session) {
    var deferred = Q.defer();
    var sid_ip_unique = { sid: sid, ip: session.geo_info.ip };
    SignInLog.findOne(sid_ip_unique, function (err, doc){
        if (err) {
            deferred.reject(err);
        } else if (doc) {
            deferred.resolve(doc);
        } else {
            new SignInLog(sid_ip_unique).save(function (err, doc) {
                if (err) {
                    deferred.reject(err);
                } else {
                    deferred.resolve(doc);
                }
            });
        }
    });
    return deferred.promise;
};

var change_user_is_online = function (uid, is_online) {
    User.findById(uid, function (err, user) {
       if (err && !user) return;
       user.is_online = is_online;
       user.save()
    });
};


exports.save_sign_in_log = function (sid, session) {
    get_sign_in_log(sid, session).then(function (doc) {
        doc.user = session.user.id;
        doc.city = session.geo_info.city;
        doc.user_agent = session['user-agent'];
        doc.oauth_type = session.user.oauth_type;
        doc.cookie_expires = session.cookie.expires;
        doc.is_online = true;
        doc.save(function (err, doc) {
            if (!err && doc) change_user_is_online(doc.user, true);
        });
    }).done();
};

exports.offline_sign_in_log = function (sid, ip) {
    SignInLog.findOne({sid: sid, ip: ip}, function (err, doc){
        if (err || !doc) return;
        doc.is_online = false;
        doc.save(function (err, doc) {
            SignInLog.count({user: doc.user, is_online: true, cookie_expires: {$gt: new Date()}}, function (err, count) {
                if (!err && (count == 0)) change_user_is_online(doc.user, false);
            });
        });
    });
};