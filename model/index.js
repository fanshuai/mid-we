var Q = require('q');
var util = require('util');
var mongoose = require('mongoose');
//var validator = require('validator');
var config = require('../config');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

mongoose.connect(
    util.format('mongodb://%s/%s', config.mongodb.host, config.mongodb.database)
);

var AutoIdSchema = new Schema({
    _id: String,
    seq: { type: Number, default: 0 }
});
var AutoId = mongoose.model('AutoId', AutoIdSchema, 'md_auto_id');

var auto_increment = function (name) {
    var deferred = Q.defer();
    AutoId.findOneAndUpdate(
        { _id: name },
        { $inc: { seq: 1 } },
        { new: true, upsert: true },
        function (err, increment) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(increment.seq + 10000000);
            }
        }
    );
    return deferred.promise;
};

var UserSchema = new Schema({
    _id: Number,
    email: { type: String, unique: true },
    username: { type: String, unique: true },
    nickname: { type: String, index: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    status: { type: String, default: 'ok' },
    is_online: {type: Boolean, default: false},
    sign_oauth_type: { type: String },
    sign_oauth_uid: { type: String },
    sign_oauth_home: { type: String },
    avatar: { type: ObjectId },
    gender: { type: String },
    brief: { type: String },
    social_network: {
        weibo: { type: Number, ref: 'OAuthProfile' },
        douban: { type: Number, ref: 'OAuthProfile' },
        google: { type: Number, ref: 'OAuthProfile' },
        github: { type: Number, ref: 'OAuthProfile' },
        facebook: { type: Number, ref: 'OAuthProfile' }
    }
});

//UserSchema.path('email').validate(function (email) {
//    return validator.isEmail(email);
//}, 'The e-mail field cannot be empty.');

//  init, validate, save and remove
UserSchema.pre('save', function (next) {
    var doc = this;
    if (doc.isNew) {
        auto_increment('user_id').then(function (auto_id) {
            doc._id = auto_id;
            doc.email = '@' + doc._id;
            doc.username = '~' + doc._id;
            next();
        }).catch(function (err) {
            next(err)
        }).done();
    } else {
        doc.updated_at = new Date();
        next();
    }
});


var OAuthProfileSchema = new Schema({
    _id: Number,
    user: { type: Number, ref: 'User' },
    type: String,
    uid: String,
    token_info: {
        access_token: String,
        expires_in: Number
    },
    token_updated: Date,
    profile: {
        username: String,
        nickname: String,
        gender: String,
        email: String,
        location: String,
        desc: String,
        avatar: String,
        home_url: String,
        created_at: Date
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

OAuthProfileSchema.index({type: 1, uid: 1}, {unique: true});

OAuthProfileSchema.pre('save', function (next) {
    var doc = this;
    if (doc.isNew) {
        auto_increment('auth_profile_id').then(function (auto_id) {
            doc._id = auto_id;
            next();
        }).catch(function (err) {
            next(err)
        }).done();
    } else {
        doc.updated_at = new Date();
        next();
    }
});

function setLatLng(num) {
    return num.toFixed(6);
}

var TopicSchema = new Schema({
    _id: Number,
    user: { type: Number, ref: 'User' },
    title: { type: String, index: true },
    content: { type: String, index: false },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    status: { type: String, default: 'ok' },
    images: [{ type: ObjectId }],
    geo_info: {
        ip: { type: String },
        country: { type: String },
        region: { type: String },
        city: { type: String },
        lat: { type: Number, set: setLatLng },
        lng: { type: Number, set: setLatLng }
    },
    loc_info: {
        lat: { type: Number, set: setLatLng },
        lng: { type: Number, set: setLatLng },
        accuracy: { type: Number },
        select: { type: Boolean, default: false }
    },
    views: { type: Number, default: 0 },
    comments: { type: Number, default: 0 },
    likes: {
        up: [ { type:Number, ref:'User' } ],
        down: [ { type:Number, ref:'User' } ]
    },
    report: [
        {
            user: { type: Number, ref: 'User' },
            reason: { type: String }
        }
    ]
});

TopicSchema.pre('save', function (next) {
    var doc = this;
    if (doc.isNew) {
        auto_increment('topic_id').then(function (auto_id) {
            doc._id = auto_id;
            next();
        }).catch(function (err) {
            next(err)
        }).done();
    } else {
        doc.updated_at = new Date();
        next();
    }
});


var CommentSchema = new Schema({
    _id: Number,
    user: { type: Number, ref: 'User' },
    topic: { type: Number, ref: 'Topic' },
    content: { type: String, index: false },
    created_at: { type: Date, default: Date.now },
    status: { type: String, default: 'ok' },
    geo_info: {
        ip: String,
        country: String,
        region: String,
        city: String,
        lat: Number,
        lng: Number
    },
    loc_info: {
        lat: Number,
        lng: Number,
        accuracy: Number
    },
    likes: {
        up: [ { type:Number, ref:'User' } ],
        down: [ { type:Number, ref:'User' } ]
    },
    report: [
        {
            user: { type: Number, ref: 'User' },
            reason: { type: String }
        }
    ]
});

CommentSchema.pre('save', function (next) {
    var doc = this;
    if (doc.isNew) {
        auto_increment('comment_id').then(function (auto_id) {
            doc._id = auto_id;
            next();
        }).catch(function (err) {
            next(err)
        }).done();
    } else {
        next();
    }
});


var MessageSchema = new Schema({
    _id: Number,
    user: { type: Number, ref: 'User' },
    to_user: { type: Number, ref: 'User' },
    contact_key: {type: String, index: true},
    content: { type: String, index: false },
    created_at: { type: Date, default: Date.now },
    status: { type: String, default: 'ok' },
    image: { type: ObjectId },
    city: { type: String },
    read_time: { type: Date }
});

MessageSchema.pre('save', function (next) {
    var doc = this;
    if (doc.isNew) {
        doc.contact_key = [doc.user, doc.to_user].sort().join('_');
        auto_increment('message_id').then(function (auto_id) {
            doc._id = auto_id;
            next();
        }).catch(function (err) {
            next(err)
        }).done();
    } else {
        next();
    }
});


var SignInLogSchema = new Schema({
    ip: { type: String },
    sid: { type: String },
    city: { type: String },
    oauth_type: { type: String },
    user_agent: { type: String },
    user: { type: Number, ref: 'User' },
    is_online: {type: Boolean, default: false},
    sign_time: { type: Date, default: Date.now },
    cookie_expires: {type: Date}
});

SignInLogSchema.index({ip: 1, sid: 1}, {unique: true});

exports.User = mongoose.model('User', UserSchema, 'md_user');
exports.OAuthProfile = mongoose.model('OAuthProfile', OAuthProfileSchema, 'md_oauth_profile');


exports.Topic = mongoose.model('Topic', TopicSchema, 'md_topic');
exports.Comment = mongoose.model('Comment', CommentSchema, 'md_comment');
exports.Message = mongoose.model('Message', MessageSchema, 'md_message');

exports.SignInLog = mongoose.model('SignInLog', SignInLogSchema, 'md_sign_in_log');


var FsFilesSchema = new Schema({
    filename: String,
    contentType: String,
    length: Number,
    chunkSize: Number,
    uploadDate: Date,
    aliases: String,
    metadata: { },
    md5: String
});

exports.FsFiles = mongoose.model('FsFiles', FsFilesSchema, 'fs.files');

// user

// talk

// mass

// login