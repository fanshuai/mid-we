requirejs.config({

    baseUrl: '/static/',

    waitSeconds: 60,

    paths: {
        'jquery': 'bower/jquery/dist/jquery.min',
        'bootstrap': 'bower/bootstrap/dist/js/bootstrap.min',

        'angular': 'bower/angular/angular.min',
        'angularRoute': 'bower/angular-route/angular-route.min',
        'angularAnimate': 'bower/angular-animate/angular-animate.min',
        'angularSanitize': 'bower/angular-sanitize/angular-sanitize.min',

        'leaflet': 'bower/leaflet/dist/leaflet',
        'leafletLabel': 'bower/Leaflet.label/dist/leaflet.label',
        'leafletMarkercluster': 'bower/leaflet.markercluster/dist/leaflet.markercluster',
        'angularLeaflet': 'bower/angular-leaflet-directive/dist/angular-leaflet-directive',


        'app': 'app-ng/app',
        'config': 'app-ng/config',
        'routes': 'app-ng/routes',
        'modal': 'app-ng/modules/modal',
        'filters': 'app-ng/modules/filters',
        'services': 'app-ng/modules/services',
        'directives': 'app-ng/modules/directives',

        'baseController': 'app-ng/controllers/baseController',
        'indexController': 'app-ng/controllers/indexController',
        'nearbyController': 'app-ng/controllers/nearbyController',
        'messageController': 'app-ng/controllers/messageController',

        'instant': 'javascripts/instant',
        'socket.io': 'javascripts/socket.io/socket.io',
        'socket.io-stream': 'javascripts/socket.io/socket.io-stream'
    },

    shim: {
        'jquery': {
            exports: 'jQuery'
        },
        'socket.io-stream': {
            deps: [ 'socket.io' ]
        },
        'bootstrap': {
            deps: [ 'jquery' ]
        },
        'angular': {
            deps: [ 'jquery' ],
            exports: 'angular'
        },
        'angularRoute': {
            deps: [ 'angular' ]
        },
        'angularAnimate': {
            deps: [ 'angular' ]
        },
        'angularSanitize': {
            deps: [ 'angular' ]
        },
        'leafletLabel': {
            deps: [ 'leaflet' ]
        },
        'leafletMarkercluster': {
            deps: [ 'leaflet' ]
        },
        'angularLeaflet': {
            deps: [ 'angular', 'leaflet', 'leafletLabel', 'leafletMarkercluster' ]
        }
    }
});

require([
    'angular',
    'config',
    'routes',
    'filters',
    'services',
    'directives',
    'bootstrap',
    'instant'
], function(angular) {
    'use strict';
    angular.bootstrap(document, ['midwe-app']);
});
