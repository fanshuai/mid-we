define(['app'], function (app) {

    'use strict';

    app.directive('ngsImage', ['socketStream', function (socketStream) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.addClass('image-loading');
                var has_size = element.attr('width') || element.attr('height');
                if (!has_size) {
                    element.attr('width', 128);
                    element.attr('height', 128);
                }
                var image_callback = function (base_image) {
                    if (!has_size) {
                        element.removeAttr('width');
                        element.removeAttr('height');
                    }
                    element.attr('src', 'data:image/jpeg;base64,' + base_image);
                    element.removeClass('image-loading');
                    element.data('loaded', true);
                };
                var get_image = function (image_id) {
                    if (!image_id) return;
                    socketStream.image({_id: image_id}, image_callback);
                };
                scope.$watch(function () {
                    return attrs['ngsImage'];
                }, get_image, true);
                element.on('click', function () {
                    if (!element.data('loaded')) get_image(attrs['ngsImage']);
                });
            }
        }
    }]);

    app.directive('ngsComposeImage', ['$rootScope', 'socketStream', function ($rootScope, socketStream) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                scope.upload_images = {};
                var elInput = element.find('input:first');
                var elName = element.find('[data-name]:first');
                var elPreview = element.find('[data-preview]:first');
                var elWarning = element.find('[data-warning]:first');
                var elFinished = element.find('[data-finished]:first');
                elInput.change(function(e) {
                    elWarning.text('');
                    if (e.target.files && e.target.files[0]) {
                        var file = e.target.files[0];
                        if (file.size > 524288) {
                            elWarning.text('Image must be less than 512 KB');
                        } else if (scope.upload_images[file.name]) {
                            elWarning.text(file.name.substr(-20) + ' already exists');
                        } else if (!($rootScope.topic_compose.images.length < 5)) {
                            elWarning.text('More then 5 images');
                        } else {
                            $rootScope.compose_uploading = true;
                            var reader = new FileReader();
                            var elImg = $('<img>', {title: 'Remove ' + file.name});
                            reader.onload = function (e) {
                                elImg.attr('src', e.target.result);
                                elPreview.append(elImg);
                            };
                            reader.readAsDataURL(file);
                            socketStream.uploadImage(file, function (finished) {
                                elFinished.text(finished + '%');
                            }, function (data) {
                                elImg.tooltip();
                                elInput.val('');
                                elFinished.text('');
                                $rootScope.compose_uploading = false;
                                $rootScope.topic_compose.images.push(data._id);
                                scope.upload_images[file.name] = data._id;
                                elImg.css('opacity', 1);
                                elImg.on('click', function (e) {
                                    e.target.remove();
                                    elImg.tooltip('destroy');
                                    delete scope.upload_images[file.name];
                                    var index = $rootScope.topic_compose.images.indexOf(data._id);
                                    if (index > -1) {
                                        $rootScope.topic_compose.images.splice(index, 1);
                                    }
                                    scope.$$phase || scope.$apply();
                                    socketStream.remove(data._id);
                                });
                            });
                        }
                    }
                });
                $rootScope.$watch('topic_compose.images', function (images) {
                    if (images.length == 0){
                        elName.text('Add photo');
                    } else if (images.length < 5) {
                        elName.text('Add more');
                    } else {
                        elName.text(images.length + ' added');
                    }
                }, true);
            }
        }
    }]);

    app.directive('ngsCommentLikes', ['socket', function (socket) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                var getCommentLike = function () {
                    socket.emit('comment.likes', {_id: scope.comment._id}, function (data) {
                        if (data && !data.error) scope.comment.likes = data;
                    });
                };
                scope.$watch(function () {
                    return new Date().getMinutes();
                }, function () {
                    getCommentLike();
                }, true);
                element.on('mouseover', function () {
                    getCommentLike();
                });
            }
        }
    }]);

    app.directive('ngsMessage', ['$rootScope', '$modal', 'messageService', function ($rootScope, $modal, messageService) {
        var messageModal = $modal({
            show: false,
            keyboard: false,
            scope: $rootScope,
            template: '/static/templates/common/message.html'
        });
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    messageService.show(attrs.ngsMessage);
                    messageModal.$promise.then(messageModal.show);
                });
            }
        }
    }]);

    app.directive('ngsContact', ['socket', function (socket) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var to_user_id = scope.$eval(attrs.ngsContact);
                socket.emit('message.to_user', {to_user: to_user_id}, function (data) {
                    if (data && !data.error) scope.contact.other_user = data;
                });
                socket.emit('message.unread', {to_user: to_user_id}, function (data) {
                    if (!(data || {}).error) scope.contact.unread = data;
                });
            }
        }
    }]);

    app.directive('ngsLocation', ['geolocation', function (geolocation) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var elIcon = element.find('.glyphicon:first');
                var elAddress = element.find('[data-address]:first');
                var elDistance = element.find('[data-distance]:first');
                elDistance.text(' · · · ');
                scope.$watch(function () {
                    return scope.$eval(attrs.ngsLocation);
                }, function (loc) {
                    if (loc && loc.lat && loc.lng){
                        geolocation.getDistance(loc).success(function (distance) {
                            elDistance.text(distance + ' km');
                        }).error(function (err) {
                            elDistance.text('x km');
                            console.log(err);
                        });
                    } else {
                        element.remove();
                    }
                }, true);
                element.on('mouseover', function () {
                    var loc_info = scope.$eval(attrs.ngsLocation);
                    if (elAddress.text() || !loc_info || !loc_info.lat || !loc_info.lng) return;
                    elIcon.addClass('default-spin');
                    geolocation.codeLatLng(loc_info).success(function (address) {
                        elAddress.text(address + ' · ');
                        elIcon.removeClass('default-spin');
                    }).error(function () {
                        elIcon.removeClass('default-spin');
                    });
                });
                scope.$watch('$rootScope.loc_info', function (user_loc_info) {
                    var loc_info = scope.$eval(attrs.ngsLocation);
                    if (loc_info && loc_info.accuracy && user_loc_info){
                        geolocation.getDistance(loc_info).success(function (distance) {
                            elDistance.text(distance + ' km');
                        });
                    }
                }, true);
            }
        }
    }]);

    app.directive('ngsAddress', ['geolocation', function (geolocation) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.ngsAddress);
                }, function (loc) {
                    if (loc && loc.lat && loc.lng){
                        element.text(' · · · ');
                        geolocation.codeLatLng(loc).success(function (formatted_address) {
                            element.text(formatted_address);
                        }).error(function () {
                            element.text('unknown');
                        });
                    } else {
                        element.text('disabled');
                    }
                }, true);
            }
        }
    }]);

    app.directive('ngsDistance', ['geolocation', function (geolocation) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.ngsDistance);
                }, function (loc) {
                    if (loc && loc.lat && loc.lng){
                        geolocation.getDistance(loc).success(function (distance) {
                            element.text(' · ' + distance + ' km');
                            attrs.$set('ngsDistance', '');
                        }).error(function () {
                            attrs.$set('ngsDistance', '');
                        });
                    }
                }, true);
            }
        }
    }]);

    app.directive('ngsPastTime', ['pastTime', function (pastTime) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('mouseover', function () {
                    element.text(pastTime(attrs['ngsPastTime']));
                    element.tooltip('show');
                });
                scope.$watch(function () {
                    return new Date().getMinutes();
                }, function () {
                    element.text(pastTime(attrs['ngsPastTime']));
                }, true);
            }
        }
    }]);

    app.directive('ngsTitle', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('mouseover', function () {
                    element.tooltip('show');
                });
                scope.$watch(function () {
                    return attrs['ngsTitle']
                }, function (title) {
                    attrs.$set('title', title);
                }, true);
            }
        }
    });

    app.directive('ngsScrollBottom', function(){
        return {
            priority: 1,
            require: ['?ngModel'],
            restrict: 'A',
            link: function(scope, $el, attrs, ctrls){
                var el = $el[0],
                    ngModel = ctrls[0] || {
                        $setViewValue: function(value){
                            this.$viewValue = value;
                        },
                        $viewValue: true
                    };

                function scrollToBottom(){
                    el.scrollTop = el.scrollHeight;
                }

                function shouldActivateAutoScroll(){
                    // + 1 catches off by one errors in chrome
                    return el.scrollTop + el.clientHeight + 1 >= el.scrollHeight;
                }

                scope.$watch(function(){
                    if(ngModel.$viewValue){
                        scrollToBottom();
                    }
                });

                $el.bind('scroll', function(){
                    var activate = shouldActivateAutoScroll();
                    if(activate !== ngModel.$viewValue){
                        scope.$apply(ngModel.$setViewValue.bind(ngModel, activate));
                    }
                });
            }
        };
    });

});