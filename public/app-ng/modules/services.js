define(['app', 'socket.io', 'socket.io-stream' ], function (app, io, ss) {

    'use strict';

    var deferredExtend = function (deferred) {
        deferred.promise.success = function (fn) {
            deferred.promise.then(function (value) {
                fn(value);
            });
            return deferred.promise;
        };
        deferred.promise.error = function (fn) {
            deferred.promise.then(null, function (value) {
                fn(value);
            });
            return deferred.promise;
        };
        return deferred.promise;
    };

    app.service('socket', ['$q', '$rootScope', function ($q, $rootScope) {
        var socket = io();
        this.on = function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        };
        this.emit = function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
                if (args.error) console.log(eventName, data, args);
            })
        };
        this.sign_in = function () {
            var deferred = $q.defer();
            this.emit('user.sign-in', '', function (data) {
                if (data && data.user) {
                    deferred.resolve(data);
                } else if (data.oauth_type) {
                    deferred.resolve(data);
                } else {
                    deferred.reject(data);
                }
            });
            return deferredExtend(deferred);
        };
    }]);

    app.service('socketStream', ['$rootScope', '$cacheFactory', function ($rootScope, $cacheFactory) {
        var socket = io.connect('/stream');
        var image_cache = $cacheFactory('image_cache');

        this.uploadImage = function (file, progress, callback) {
            var finished_size = 0;
            var stream = ss.createStream();
            ss(socket).emit('uploadImage', stream, {name: file.name, type: file.type}, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(ss(socket), args);
                    }
                });
            });
            // ss.createBlobReadStream(file).pipe(stream);
            var blobStream = ss.createBlobReadStream(file);
            blobStream.on('data', function(chunk) {
                finished_size += chunk.length;
                $rootScope.$apply(function () {
                    progress(Math.floor(finished_size / file.size * 100));
                });
            });
            blobStream.pipe(stream);
        };

        this.image = function (data, callback) {
            if (!data || !data._id) return;
            var base_string = image_cache.get(data._id) || '';
            if (base_string && callback) return callback(base_string);
            var stream = ss.createStream();
            ss(socket).emit('image', stream, data);
            stream.on('data', function(chunk) {
                base_string += chunk.toString('base64');
            });
            stream.on('end', function () {
                image_cache.put(data._id, base_string);
                $rootScope.$apply(function () {
                    if (callback) {
                        callback(base_string);
                    }
                });
            });
        };

        this.remove = function (fid, callback) {
            socket.emit('remove', fid, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        };

    }]);

    app.service('geolocation', ['$q', '$window', '$rootScope', 'socket', 'appConfig', function ($q, $window, $rootScope, socket, appConfig) {

        this.codeLatLng = function (coords) {
            var deferred = $q.defer();
            socket.emit('osm.reverse', coords, function (data) {
                if (data && data.address) {
                    deferred.resolve(data.address);
                } else {
                    deferred.reject(data);
                }
            });
            return deferredExtend(deferred);
        };

        this.getPosition = function () {
            var deferred = $q.defer();
            if ($rootScope.loc_info && ($rootScope.loc_info.timestamp > (new Date() - appConfig.position_cachetime))) {
                deferred.resolve($rootScope.loc_info);
            } else if ($window.navigator && $window.navigator.geolocation) {
                $window.navigator.geolocation.getCurrentPosition(function(position) {
                    if (position.coords.accuracy < appConfig.accuracy_standard) {
                        $rootScope.loc_info = {
                            accuracy: position.coords.accuracy,
                            timestamp: position.timestamp,
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        deferred.resolve($rootScope.loc_info);
                    } else {
                        deferred.reject('Need more accurate values.');
                    }
                }, function(err) {
                    deferred.reject(err);
                }, { timeout: appConfig.geolocation_timeout });
            } else {
                deferred.reject('Geolocation is not suppported in your browser.');
            }
            return deferredExtend(deferred);
        };

        var get_distance = function(p1, p2) {
            var rad = function(x) {
                return x * Math.PI / 180;
            };
            var dLat = rad(p2.lat - p1.lat);
            var dLong = rad(p2.lng - p1.lng);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
            return 6378137 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        };

        this.getDistance = function(target) {
            var deferred = $q.defer();
            this.getPosition().success(function (position) {
                deferred.resolve(
                    Number(get_distance(target, position) / 1000).toFixed(1)
                );
            }).error(function (err) {
                deferred.reject(err);
            });
            return deferredExtend(deferred);
        };

    }]);

    app.service('topicPopup', ['$rootScope', '$modal', 'socket', 'idString', function ($rootScope, $modal, socket, idString) {
        var topicModal = $modal({
            show: false,
            keyboard: false,
            scope: $rootScope,
            template: '/static/templates/common/topic.html'
        });

        this.show = function (topic_id) {
            topicModal.$promise.then(topicModal.show);
            socket.emit('topic.get', { _id: topic_id }, function (data) {
                if (data && !data.error) {
                    data.id_string = idString(data._id);
                    $rootScope.popup_topic_info = data;
                }
            });
        };
    }]);

    app.service('composePopup', ['$rootScope', '$modal', 'socket', 'geolocation', function ($rootScope, $modal, socket, geolocation) {
        var composeModal = $modal({
            show: false,
            keyboard: false,
            scope: $rootScope,
            template: '/static/templates/common/compose.html'
        });
        $rootScope.topic_compose = {images: []};

        this.show = function () {
            $rootScope.compose_uploading = false;
            composeModal.$promise.then(composeModal.show);
            if (!$rootScope.topic_compose.loc_info) {
                geolocation.getPosition().success(function (position) {
                    $rootScope.topic_compose.loc_info = position;
                });
            }
        };

        this.post = function () {
            socket.emit('topic.add', $rootScope.topic_compose, function (data) {
                if (data && data.error) alert(data.error);
                $rootScope.topic_compose = {images: []};
                composeModal.$promise.then(composeModal.hide);
                if (data && !data.error) {
                    $rootScope.$broadcast('postComposeSuccess', data);
                }
            });
        };
    }]);

    app.service('messageService', ['$rootScope', 'socket', 'socketStream', function ($rootScope, socket, socketStream) {
        var to_user_id;
        var user_id = $rootScope.user._id;
        $rootScope.message_buffer = [];
        $rootScope.message_compose = {};
        $rootScope.message_to_user = {};

        $rootScope.messageImageChanged = function (target) {
            if (target.files && target.files[0]) {
                var file = target.files[0];
                if (file.size > 524288) return alert('Image must be less than 512 KB');
                $rootScope.message_uploading = true;
                $rootScope.messageImageUploadFinished = 0;
                var reader = new FileReader();
                reader.onload = function (e) {
                    $rootScope.messageImageUploadSrc = e.target.result;
                };
                reader.readAsDataURL(file);
                socketStream.uploadImage(file, function (finished) {
                    $rootScope.messageImageUploadFinished = finished;
                }, function (data) {
                    $rootScope.message_compose.image = data._id;
                    $rootScope.messageSend();
                });

            }
        };

        $rootScope.messageSend = function () {
            socket.emit('message.send', $rootScope.message_compose, function (data) {
                if (data && data.error) alert(data.error);
                $rootScope.message_uploading = false;
                $rootScope.messageImageUploadSrc = '';
                $rootScope.message_compose.image = '';
                $rootScope.message_compose.content = '';
            });
        };

        $rootScope.messageList = function () {
            var query = {to_user: to_user_id};
            if ($rootScope.message_buffer.length > 0) {
                query.last_id = $rootScope.message_buffer[0]._id;
            }
            socket.emit('message.list', query, function (data) {
                if (data && !data.error && (data instanceof Array)) {
                    $rootScope.message_has_more = (data.length == $rootScope.page_size);
                    $rootScope.message_buffer = data.reverse().concat($rootScope.message_buffer);
                }
            });
        };

        this.show = function (user_id) {
            to_user_id = user_id;
            $rootScope.message_buffer = [];
            $rootScope.message_compose.to_user = to_user_id;
            socket.emit('message.to_user', {to_user: user_id}, function (data) {
                if (data && !data.error) $rootScope.message_to_user = data;
            });
            socket.emit('message.list', {to_user: user_id}, function (data) {
                if (data && !data.error && (data instanceof Array)) {
                    $rootScope.message_has_more = (data.length == $rootScope.page_size);
                    $rootScope.message_buffer = data.reverse().concat($rootScope.message_buffer);
                }
            });
        };

        socket.on('message.send', function (data) {
            if (!data || data.error || !data.user || !data.to_user)
                return console.log('message.send data error', data);
            if ((data.user._id==user_id) && (data.to_user._id==to_user_id)) {
                $rootScope.message_buffer.push(data);
            } else if ((data.user._id==to_user_id) && (data.to_user._id==user_id)) {
                $rootScope.message_buffer.push(data);
            }
        });

    }]);

});