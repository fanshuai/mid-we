define(['app', 'config', 'baseController', 'indexController', 'nearbyController', 'messageController'], function (app) {

    'use strict';

    app.config(['$routeProvider', function ($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: '/static/templates/index.html',
                controller: 'IndexController',
                title: 'MidWe'
            }, '') // Invalid number of arguments, expected 3 ?
            .when('/.::id_string', {
                templateUrl: '/static/templates/topic.html',
                controller: 'TopicController',
                title: 'Topic'
            }, '')
            .when('/nearby', {
                templateUrl: '/static/templates/nearby.html',
                controller: 'NearbyController',
                reloadOnSearch: false,
                title: 'Nearby'
            }, '')
            .when('/message', {
                templateUrl: '/static/templates/message.html',
                controller: 'MessageController',
                reloadOnSearch: false,
                title: 'Nearby'
            }, '')
            .when('/me', {
                templateUrl: '/static/templates/me.html',
                controller: 'MeController',
                title: 'Me'
            }, '')
            .when('/auth-ing', {
                templateUrl: '/static/templates/auth-ing.html',
                controller: 'AuthIngController',
                title: 'Waiting'
            }, '')
            .otherwise({
                templateUrl: '/static/templates/other.html',
                controller: 'OtherController'
                // redirectTo: '/'
            });

    }]);

});
