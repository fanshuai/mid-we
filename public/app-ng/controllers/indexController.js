define(['app', 'config'], function (app) {

    'use strict';

    app.controller('IndexController', ['$scope', '$location', '$rootScope', 'socket', 'geolocation', 'idString', function ($scope, $location, $rootScope, socket, geolocation, idString) {

        $scope.here_list = [];
        $scope.topic_list = [];
        $scope.stream_item = [];
        $scope.has_more = true;
        $scope.is_loading = false;

        socket.on('topic.stream', function (data) {
            $scope.stream_item.unshift(data);
        });

        $scope.loadTopiclist = function () {
            $scope.is_loading = true;
            var query = { search: $scope.search || '' };
            if ($scope.topic_list.length > 0) {
                query.last_id = $scope.topic_list[$scope.topic_list.length - 1]._id;
            }
            socket.emit('topic.list', query, function (data) {
                if (data && !data.error && (data instanceof Array)) {
                    $scope.topic_list = $scope.topic_list.concat(data);
                    $scope.has_more = (data.length == $rootScope.page_size);
                }
                $scope.is_loading = false;
            });
        };

        $scope.queryTopiclist = function () {
            $scope.is_loading = true;
            socket.emit('topic.list', {search: $scope.search}, function (data) {
                if (data && !data.error) $scope.topic_list = data;
                $scope.has_more = (data.length == $rootScope.page_size);
                $scope.is_loading = false;
            });
        };

        $scope.loadTopiclist();

        $scope.showStream = function () {
            $scope.topic_list = $scope.stream_item.concat($scope.topic_list || []);
            $scope.stream_item = [];
        };

        $scope.topicClick = function (topic_id) {
            $location.path('/.:' + idString(topic_id));
        };

        $scope.topicMouseover = function (topic) {
            socket.emit('topic.comments', {_id: topic._id}, function (data) {
                if (data && !data.error) topic.comments = data;
            });
        };

        socket.on('i-am-here', function (data) {
            if (data && (data.city == $rootScope.user.geo_info.city)) $scope.here_list.unshift(data);
            if ($scope.here_list.length > 32) $scope.here_list = $scope.here_list.slice(0, 32);
        });

        $scope.sendHere = function (e) {
            $(e.target).button('loading');
            geolocation.getPosition().success(function (position) {
                socket.emit('i-am-here', position, function () {
                    $(e.target).button('reset');
                });
            }).error(function (err) {
                console.log(err);
                $(e.target).button('reset');
            });
        };

    }]);

    app.controller('TopicController', ['$scope', '$location', '$routeParams', 'socket', 'geolocation', 'idNumber', function ($scope, $location, $routeParams, socket, geolocation, idNumber) {
        $scope.buzz_add = {};
        $scope.buzz_list = [];
        $scope.comment_add = {};
        $scope.comment_list = [];
        $scope.topic_user = [];
        socket.emit('topic.get', { _id: idNumber($routeParams.id_string) }, function (data) {
            if (!data || data.error) {
                $location.path('/');
            } else {
                document.title = data.title;
                $scope.topic_info = data;
                socket.emit('comment.list', {topic: data._id}, function (data) {
                    if (data && !data.error) $scope.comment_list = data;
                });
                $scope.topicLike();
            }
        });

        socket.on('topic.buzz', function (data) {
            if (data && (data.topic == $scope.topic_info._id)) $scope.buzz_list.unshift(data);
            if ($scope.buzz_list.length > 32) $scope.buzz_list = $scope.buzz_list.slice(0, 32);
        });
        socket.on('topic.comment', function (data) {
            if (data && (data.topic == $scope.topic_info._id)) $scope.comment_list.push(data);
        });

        socket.on('topic.views', function (views) {
            if ($scope.topic_info) $scope.topic_info.views = views;
        });
        socket.on('topic.comments', function (comments) {
            if ($scope.topic_info) $scope.topic_info.comments = comments;
        });
        socket.on('comment.hate', function (data) {
            for (var i=0; i<$scope.comment_list.length; i++) {
                if ($scope.comment_list[i]._id == data._id) {
                    $scope.comment_list.splice(i, 1);
                    break;
                }
            }
        });
        socket.on('topic.user.change', function () {
            $scope.onlineUser();
        });
        socket.on('topic.likes.change', function () {
            $scope.topicLike();
        });
        $scope.topicLike = function (type) {
            socket.emit('topic.likes', {_id: $scope.topic_info._id, type: type || ''}, function (data) {
                if (data && !data.error) $scope.topic_info.likes = data;
            });
        };

        geolocation.getPosition().success(function (position) {
            $scope.comment_add.loc_info = position;
        });

        $scope.postComment = function () {
            $scope.comment_add.topic = $scope.topic_info._id;
            socket.emit('comment.add', $scope.comment_add, function (data) {
                if (data && !data.error) {
                    $scope.comment_add.content = '';
                    $scope.comment_add.placeholder = 'Comment success';
                }
            });
        };

        $scope.commentLike = function (comment, type) {
            socket.emit('comment.likes', {_id: comment._id, type: type}, function (data) {
                if (data && !data.error) comment.likes = data;
            });
        };

        $scope.onlineUser = function() {
            socket.emit('topic.user', {topic: $scope.topic_info._id}, function (data) {
                if (data && !data.error) $scope.topic_user = data;
            });
        };

        $scope.sendBuzz = function () {
            $scope.buzz_add.topic = $scope.topic_info._id;
            socket.emit('topic.buzz', $scope.buzz_add, function (data) {
                if (data && !data.error) {
                    $scope.buzz_add.content = '';
                }
            });
        };

        $scope.$on('$destroy', function () {
            if ($scope.topic_info) socket.emit('comment.leave', {topic: $scope.topic_info._id});
        });

    }]);

});