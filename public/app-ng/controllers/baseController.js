define(['app', 'config'], function (app) {

    'use strict';

    app.controller('NavbarController', ['$scope', '$window', '$location', '$rootScope', 'geolocation', 'composePopup', function ($scope, $window, $location, $rootScope, geolocation, composePopup) {
        $scope.active = function (viewLocation) {
            return $location.path() == viewLocation;
        };

        $scope.signOut = function () {
            $window.location = '/sign-out';
        };

        $scope.showCompose = function () {
            $rootScope.topic_compose.loc_info = null;
            composePopup.show();
        };

        $rootScope.postCompose = function () {
            composePopup.post();
        };

        geolocation.getPosition();
    }]);

    app.controller('AuthIngController', ['$location', '$rootScope', function ($location, $rootScope) {
        if ($rootScope.user) { $location.path('/'); }
    }]);

    app.controller('OtherController', ['$scope', '$location', function ($scope, $location) {
        $scope.location_path = $location.path();
        document.title = 'Not Found';
    }]);

    app.controller('FooterController', ['$scope', function ($scope) {
        $scope.now = new Date();
    }]);

    app.controller('MeController', ['$scope', '$rootScope', 'socket', 'idString', function ($scope, $rootScope, socket, idString) {
        $scope.ptype ='topic';
        $scope.idString = idString;
        $scope.topic_info = {page: 1, page_total: 1};
        $scope.comment_info = {page: 1, page_total: 1};
        $scope.getTopiclist = function (page) {
            if (page > 0) $scope.topic_info.page = page;
            if (page > $scope.topic_info.page_total) $scope.topic_info.page = $scope.topic_info.page_total;
            socket.emit('me.topic.list', {page: $scope.topic_info.page}, function (data) {
                if (data && data.error) return;
                $scope.topic_info.list = data.list || [];
                $scope.topic_info.count = (data && data.count > 0) ? data.count : 0;
                $scope.topic_info.page_total = Math.floor(($scope.topic_info.count + $rootScope.page_size - 1) / $rootScope.page_size);
            });
        };
        $scope.getCommentlist = function (page) {
            if (page > 0) $scope.comment_info.page = page;
            if (page > $scope.comment_info.page_total) $scope.comment_info.page = $scope.comment_info.page_total;
            socket.emit('me.comment.list', {page: $scope.comment_info.page}, function (data) {
                if (data && data.error) return;
                $scope.comment_info.list = data.list || [];
                $scope.comment_info.count = (data && data.count > 0) ? data.count : 0;
                $scope.comment_info.page_total = Math.floor(($scope.comment_info.count + $rootScope.page_size - 1) / $rootScope.page_size);
            });
        };

        $scope.getTopiclist(1);
        $scope.getCommentlist(1);
    }]);

});