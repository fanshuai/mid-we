define(['app', 'config'], function (app) {

    'use strict';

    app.controller('NearbyController', [
        '$scope', '$rootScope', '$modal', 'socket', 'geolocation', 'appConfig', 'topicPopup', 'composePopup', 'leafletData', 'leafletMarkersHelpers',
        function ($scope, $rootScope, $modal, socket, geolocation, appConfig, topicPopup, composePopup, leafletData, leafletMarkersHelpers) {

            $rootScope.user.geo_info.zoom = 18;
            $scope.center = $rootScope.user.geo_info;

            $scope.defaults = {
                minZoom: 5,
                maxZoom: 18,
                tileLayer: '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                tileLayerOptions: {
                    opacity: 0.8,
                    detectRetina: true,
                    reuseTiles: true,
                    attribution: appConfig.osm_attribution
                },
                doubleClickZoom: true,
                scrollWheelZoom: true,
                attributionControl: true
            };

            $scope.markers = {};

            var marker_check = function (topic) {
                if (topic._id in $scope.markers) return;
                $scope.markers[topic._id] = {
                    riseOnHover: true,
                    lat: topic.loc_info.lat,
                    lng: topic.loc_info.lng,
                    group: topic.geo_info.city,
                    label: {
                        message: topic.title,
                        options: { noHide: false, direction: 'auto', className: 'topic-label' }
                    }
                };
            };

            var update_topic_marker = function (bounds) {
                if ($scope.zoom && $scope.zoom < 16) return;
                socket.emit('topic.bound', bounds, function (data) {
                    if (!data || data.error) return;
                    for (var i=0; i<data.length; i++) {
                        marker_check(data[i]);
                    }
                });
            };

            $scope.$on('postComposeSuccess', function (event, data) {
                marker_check(data);
            });

            var compose_marker = L.marker([1, 1], {
                icon: L.icon({
                    iconUrl: '/static/images/compose.png',
                    iconSize: [36, 36],
                    iconAnchor: [18, 18],
                    popupAnchor: [0, -18],
                    labelAnchor: [15, 0]
                }),
                draggable: true
            }).bindLabel('New Topic Here', {
                className: 'compose-label'
            }).bindPopup('Zoom level not enough').on('click', function (args) {
                if ($scope.zoom && $scope.zoom > 16) {
                    compose_marker.closePopup();
                    $rootScope.topic_compose.loc_info = {
                        accuracy: new Date().getMilliseconds(),
                        lat: args.latlng.lat,
                        lng: args.latlng.lng,
                        select: true
                    };
                    composePopup.show();
                } else {
                    compose_marker.openPopup();
                }
            }).setLatLng($scope.center);

            $scope.$watch('center.zoom', function(zoom) {
                if (zoom < 16) $scope.markers = {};
                compose_marker.closePopup();
                $scope.zoom = zoom;
            });

            $scope.$on('leafletDirectiveMap.contextmenu', function(event, args){
                compose_marker.setLatLng(args.leafletEvent.latlng);
            });

            $scope.$on('leafletDirectiveMarker.click', function(event, args){
                topicPopup.show(args.markerName);
            });

            leafletData.getMap().then(function(map) {
                map.attributionControl.setPrefix(appConfig.leaflet_attribution);
                compose_marker.addTo(map);
                // map.removeLayer(compose_marker);
                update_topic_marker(map.getBounds());
                map.on('moveend', function () {
                    update_topic_marker(map.getBounds());
                });
            });

            geolocation.getPosition().success(function (position) {
                position.zoom = 18;
                $scope.center = position;
                compose_marker.setLatLng(position);
            });

            $scope.$on('$destroy', function () {
                leafletMarkersHelpers.resetMarkerGroups();
            });

        }
    ]);

});