define(['app', 'config'], function (app) {

    'use strict';

    app.controller('MessageController', ['$scope', '$rootScope', 'socket', 'geolocation', function ($scope, $rootScope, socket, geolocation) {

        $scope.here_list = [];
        $scope.contact_info = {page: 1, page_total: 1};

        socket.on('i-am-here', function (data) {
            if (data && (data.city == $rootScope.user.geo_info.city)) $scope.here_list.unshift(data);
            if ($scope.here_list.length > 32) $scope.here_list = $scope.here_list.slice(0, 32);
        });

        $scope.sendHere = function (e) {
            $(e.target).button('loading');
            geolocation.getPosition().success(function (position) {
                socket.emit('i-am-here', position, function () {
                    $(e.target).button('reset');
                });
            }).error(function (err) {
                console.log(err);
                $(e.target).button('reset');
            });
        };

        $scope.getOtherUid = function (contact) {
            return (contact.user == $rootScope.user._id) ? contact.to_user : contact.user;
        };

        $scope.getContactlist = function (page) {
            if (page > 0) $scope.contact_info.page = page;
            if (page > $scope.contact_info.page_total) $scope.contact_info.page = $scope.contact_info.page_total;
            socket.emit('message.contact', {page: $scope.contact_info.page}, function (data) {
                if (data && data.error) return;
                $scope.contact_info.list = data.list || [];
                $scope.contact_info.count = (data && data.count > 0) ? data.count : 0;
                $scope.contact_info.page_total = Math.floor(($scope.contact_info.count + $rootScope.page_size - 1) / $rootScope.page_size);
            });
        };

        $scope.$on('messageUnreadChange', function () {
            $scope.getContactlist($scope.contact_info.page);
        });

        $scope.getContactlist(1);
    }]);

});