define(['angular', 'angularRoute', 'angularAnimate', 'angularSanitize', 'angularLeaflet'], function (angular) {

    'use strict';

    return angular.module('midwe-app', ['ngRoute', 'ngAnimate', 'ngSanitize', 'leaflet-directive']);
});