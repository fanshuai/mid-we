define(['app', 'modal', 'services'], function (app) {

    'use strict';

    app.config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode({enabled: true, requireBase: false});
    }]);

    app.run(['$http', '$route', '$location', '$timeout', '$window', '$rootScope', '$templateCache', 'socket', function ($http, $route, $location, $timeout, $window, $rootScope, $templateCache, socket) {
        // OAuth timeout auto sign out
        var oauth_timeout = $timeout(function () {
            $window.location = '/sign-out';
        }, 60000);

        var user_sign_in = function () {
            socket.sign_in().success(function (data) {
                if (data.user) {
                    $rootScope.user = data.user;
                    $timeout.cancel(oauth_timeout);
                    $rootScope.page_size = data.page_size || 10;
                    if (($route.current.$$route || {}).originalPath === '/auth-ing') {
                        $location.url(data.next_url || '/');
                    } else {
                        $route.reload();
                    }
                } else {
                    $rootScope.oauth_type = data.oauth_type;
                }
            }).error(function () {
                $window.location = '/sign-out';
            });
        };

        socket.on('oauth.success', function () {
            user_sign_in();
        });

        socket.on('user.sign-out', function () {
            $window.location = '/';
        });

        $rootScope.$on('$routeChangeStart', function(event, next) {
            var auth_ing_template_url = '/static/templates/auth-ing.html';
            if (!$rootScope.user && !(next.templateUrl == auth_ing_template_url)) {
                $location.path('/auth-ing');
            } else if ($rootScope.user && (next.templateUrl == auth_ing_template_url)) {
                $location.path('/');
            }
        });

        $rootScope.$on('$routeChangeSuccess', function() {
            if ($route.current.$$route) {
                document.title = $route.current.$$route.title || document.title;
            }
        });

        socket.emit('console.out', '', function (msg) {
            console.log(msg);
        });

        socket.on('connect', function () {
            user_sign_in();
            $rootScope.offline = null;
        });

        socket.on('error', function (err) {
            $rootScope.offline = 'websocket connect failed: ' + String(err);
        });
        socket.on('disconnect', function (msg) {
            $rootScope.offline = 'websocket disconnect: ' + msg;
        });
        socket.on('console', function (msg) { console.info(msg); });

        socket.on('message.unread', function (count) {
            $rootScope.message_unread = count;
            $rootScope.$broadcast('messageUnreadChange', count);
        });

        $window.echo = function (data) {
            socket.emit('console', data, function (msg) { console.info(msg); });
            if (!data && $rootScope.loc_info) return $rootScope.loc_info;
        };

        $rootScope.contentDeal = function (content) {
            return (content || '').replace(/\n|\s{2,}/g, function (matched) {
                return matched === '\n' ? '<br>' : matched.replace(/\s/g, '&nbsp;');
            });
        };

        (function () {
            for (var route_key in $route.routes) {
                if ($route.routes.hasOwnProperty(route_key)){
                    var templateUrl = $route.routes[route_key].templateUrl;
                    if (templateUrl) { $http.get(templateUrl, { cache: $templateCache }); }
                }
            }
            var topicTemplate = '/static/templates/common/topic.html';
            $http.get(topicTemplate).then(function(res) {
                if(angular.isObject(res)) {$templateCache.put(topicTemplate, res.data)}
            });
        })();

    }]);

    app.constant('pastTime', function (datetime) {
        var diffNumber;
        datetime = new Date(datetime);
        var diffSeconds = Math.abs(Math.round((new Date() - datetime) / 1000));
        if (isNaN(diffSeconds)) {
            return '';
        } else if (Math.floor(diffSeconds / 86400) > 0) {
            return datetime.getMonth() + 1 + '-' + datetime.getDate();
        } else if (Math.floor(diffSeconds / 3600) > 0) {
            diffNumber =  Math.floor(diffSeconds / 3600);
            return diffNumber + (diffNumber == 1 ? ' hour' : ' hours');
        } else if (Math.floor(diffSeconds / 60) > 0) {
            diffNumber =  Math.floor(diffSeconds / 60);
            return diffNumber + (diffNumber == 1 ? ' minute' : ' minutes');
        } else {
            if (diffSeconds > 30){
                return diffSeconds + ' seconds';
            } else {
                return 'now';
            }
        }
    });

    app.constant('idString', function (n) {
        return Number(n).toString(2).replace(/0|1/g, function(matched){
            return {'0': '.', '1': ':'}[matched] || '';
        });
    });

    app.constant('idNumber', function (s) {
        return parseInt(String(s).replace(/\.|:/g, function(matched){
            return {'.': '0', ':': '1'}[matched] || '';
        }), 2);
    });

    app.constant('appConfig', {
        accuracy_standard: 2000,
        position_cachetime: 60000,
        geolocation_timeout: 20000,
        leaflet_attribution: '<a href="http://leafletjs.com" target="_blank">Leaflet</a>',
        osm_attribution: '© <a href="http://www.osm.org/copyright" target="_blank">OpenStreetMap</a> contributors'
    });



});