define(['jquery'], function ($) {

    'use strict';

    var getCookie = function (name) {
        var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
        return r ? r[1] : undefined;
    };

    var getQueryJson = function (query) {
        query = query.substr(1);
        var data = query.split("&");
        var result = {};
        for(var i=0; i<data.length; i++) {
            var item = data[i].split("=");
            result[item[0]] = item[1];
        }
        return result;
    };

    var enableBackToTop = function () {
        var backToTop = $('<div>', { id: 'back-to-top'});
        var backIcon = $('<span>', { class: 'glyphicon glyphicon-chevron-up' });

        backToTop.appendTo('body');
        backIcon.appendTo(backToTop);

        backToTop.hide();

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                backToTop.fadeIn ();
            } else {
                backToTop.fadeOut ();
            }
        });

        backToTop.click (function (e) {
            e.preventDefault ();

            $('body, html').animate({
                scrollTop: 0
            }, 600);
        });
    };

    $(function () {
        $.extend({
            getCookie: getCookie,
            getQueryJson: getQueryJson
        });
        enableBackToTop();
    });

});
