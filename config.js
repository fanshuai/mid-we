var path = require('path');
var debug = require('debug')('midwe:config');

var env_choices = ['production', 'development'];

if (env_choices.indexOf(process.env.NODE_ENV) < 0) {
    process.env.NODE_ENV = (process.argv[2] === 'action' ? env_choices[0] : env_choices[1]);
}

var base = process.env.NODE_ENV.substr(0, 3);
var config = require('./config/config-' + base);
config.oauth = require('./config/oauth-' + base);
config.static_dir = path.join(
    __dirname, base === 'pro' ? 'build' : 'public'
);

debug('pid:' + process.pid, 'use config:', base, 'static dir:', config.static_dir);

module.exports = config;